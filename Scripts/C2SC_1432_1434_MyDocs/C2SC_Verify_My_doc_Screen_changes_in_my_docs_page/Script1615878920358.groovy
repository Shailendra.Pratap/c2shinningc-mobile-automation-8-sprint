import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_MyProfilePage/tab_MyDocs'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MyProfilePage/tab_MyDocs'), GlobalVariable.intWaitTime)

Mobile.delay(10)

Mobile.verifyElementText(findTestObject('Object Repository/C2CS_MyDoc/txt_Contract'), 'Contract Documents')

//Mobile.verifyElementVisible(findTestObject('Object Repository/C2CS_MyDoc/btn_UpArrow'), GlobalVariable.intWaitTime)

//Mobile.tap(findTestObject('Object Repository/C2CS_MyDoc/btn_UpArrow'), GlobalVariable.intWaitTime,FailureHandling.OPTIONAL)

//Mobile.scrollToText('Loan Estimate',FailureHandling.OPTIONAL)

Mobile.verifyElementText(findTestObject('Object Repository/C2CS_MyDoc/txt_LoanEstimate'), 'Loan Estimate')

//Mobile.scrollToText('Inspection',FailureHandling.OPTIONAL)

Mobile.verifyElementText(findTestObject('Object Repository/C2CS_MyDoc/txt_Inspection'), 'Inspection')

//Mobile.scrollToText('Appraisal',FailureHandling.OPTIONAL)

Mobile.verifyElementText(findTestObject('Object Repository/C2CS_MyDoc/txt_Appraisal'), 'Appraisal',FailureHandling.OPTIONAL)

Mobile.scrollToText('Others',FailureHandling.OPTIONAL)

Mobile.verifyElementText(findTestObject('Object Repository/C2CS_MyDoc/txt_ClosingDisclosure'), 'Closing Disclosure',FailureHandling.OPTIONAL)

//Mobile.scrollToText('Wire Instructions',FailureHandling.OPTIONAL)

Mobile.verifyElementText(findTestObject('Object Repository/C2CS_MyDoc/txt_ WireInstructions'), 'Wire Instructions',FailureHandling.OPTIONAL)

//Mobile.scrollToText('Others',FailureHandling.OPTIONAL)

Mobile.verifyElementText(findTestObject('Object Repository/C2CS_MyDoc/txt_Others'), 'Others',FailureHandling.OPTIONAL)

Mobile.closeApplication()

