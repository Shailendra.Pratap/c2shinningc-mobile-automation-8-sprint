import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


/**
 * @author MohanPri
 */
WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_HomePage/txt_My Team'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_HomePage/txt_My Team'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_HomePage/txt_My Team'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_MyTeamPage/txt_TitleClosing'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/txt_TitleClosing'), GlobalVariable.intWaitTime)

//Customer already selected  preferred service provider
if(Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_MyTeamPage/btn_myTeam_btn_edit'),GlobalVariable.intWaitTime,FailureHandling.OPTIONAL)){
	
	Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/btn_myTeam_btn_edit'),GlobalVariable.intWaitTime)
	
	Mobile.delay(GlobalVariable.intWaitTime);
	
	Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_MyTeamPage/txt_WhoWouldYouLikeToAdd'), GlobalVariable.intWaitTime,FailureHandling.OPTIONAL)
	
	Mobile.tapAndHold(findTestObject('Object Repository/C2SC_MyTeamPage/input_CompanyName'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)
	
	Mobile.setText(findTestObject('Object Repository/C2SC_MyTeamPage/input_CompanyName'),'TCS',GlobalVariable.intWaitTime)
	
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime,FailureHandling.CONTINUE_ON_FAILURE)
	
	Mobile.tapAndHold(findTestObject('Object Repository/C2SC_MyTeamPage/input_ContactName'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)
	
	Mobile.setText(findTestObject('Object Repository/C2SC_MyTeamPage/input_ContactName'), 'Ann Tester', GlobalVariable.intWaitTime)
	
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime,FailureHandling.CONTINUE_ON_FAILURE)
	
	Mobile.delay(GlobalVariable.intWaitTime)
	
	Mobile.tapAndHold(findTestObject('Object Repository/C2SC_MyTeamPage/input_Email'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)
	
	String email=CustomKeywords.'com.Utilities.Utility.getRandomEmail'()
	
	String getRandomEmailId=email.toLowerCase()
	
	Mobile.setText(findTestObject('C2SC_AddTeamMemberScreen/input_email'),getRandomEmailId, GlobalVariable.intWaitTime)
	
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime,FailureHandling.CONTINUE_ON_FAILURE)
	
	Mobile.tapAndHold(findTestObject('Object Repository/C2SC_MyTeamPage/input_PhoneNumber'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)
	
	Mobile.setText(findTestObject('Object Repository/C2SC_MyTeamPage/input_PhoneNumber'), '1234567890', GlobalVariable.intWaitTime)
	
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime,FailureHandling.CONTINUE_ON_FAILURE)
	
	Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/btn_AddSPPlusIcon'), GlobalVariable.intWaitTime)
	
	Mobile.delay(GlobalVariable.intWaitTime);
	
	Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_MyTeamPage/img_verifyTitle'), GlobalVariable.intWaitTime)
	
	Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/txt_TitleClosing'), GlobalVariable.intWaitTime)
	
}else if(Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_MyTeamPage/txt_WhoWouldYouLikeToAdd'), GlobalVariable.intWaitTime,FailureHandling.OPTIONAL)){


Mobile.tapAndHold(findTestObject('Object Repository/C2SC_MyTeamPage/input_CompanyName'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_MyTeamPage/input_CompanyName'),'TCS',GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('Object Repository/C2SC_MyTeamPage/input_ContactName'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_MyTeamPage/input_ContactName'), 'Ann Tester', GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_AddTeamMemberScreen/input_email'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

String email=CustomKeywords.'com.Utilities.Utility.getRandomEmail'()

String getRandomEmailId=email.toLowerCase()

Mobile.setText(findTestObject('C2SC_AddTeamMemberScreen/input_email'),getRandomEmailId, GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_AddTeamMemberScreen/input_phone'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_AddTeamMemberScreen/input_phone'), '1234567890', GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/btn_AddSPPlusIcon'), GlobalVariable.intWaitTime)

Mobile.delay(GlobalVariable.intWaitTime);

Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_MyTeamPage/img_verifyTitle'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/txt_TitleClosing'), GlobalVariable.intWaitTime)

}

Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_MyTeamPage/btn_Delete'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_MyTeamPage/btn_Delete'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_MyTeamPage/btn_Edit'),GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/btn_Edit'),GlobalVariable.intWaitTime)

String contactNameIsEditable=Mobile.getAttribute(findTestObject('Object Repository/C2SC_MyTeamPage/input_ContactName'),'enabled', GlobalVariable.intWaitTime)

println("ContactName Field is Editable"+contactNameIsEditable)

Mobile.tapAndHold(findTestObject('Object Repository/C2SC_MyTeamPage/input_ContactName'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.delay(GlobalVariable.intWaitTime)

Mobile.clearText(findTestObject('Object Repository/C2SC_MyTeamPage/input_ContactName'), GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_MyTeamPage/input_ContactName'), 'Tester', GlobalVariable.intWaitTime)

String emailIsEditable=Mobile.getAttribute(findTestObject('Object Repository/C2SC_MyTeamPage/input_Email'),'enabled', GlobalVariable.intWaitTime)

println("Email Field is Editable"+emailIsEditable)

//Mobile.tapAndHold(findTestObject('C2SC_AddTeamMemberScreen/input_email'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)
//
//Mobile.clearText(findTestObject('C2SC_AddTeamMemberScreen/input_email'), GlobalVariable.intWaitTime)
//
//String emailID=CustomKeywords.'com.Utilities.Utility.getRandomEmail'()
//
//String RandomEmailId=emailID.toLowerCase()
//
//Mobile.setText(findTestObject('C2SC_AddTeamMemberScreen/input_email'),RandomEmailId, GlobalVariable.intWaitTime)

String phoneNumberIsEditable=Mobile.getAttribute(findTestObject('Object Repository/C2SC_MyTeamPage/input_PhoneNumber'),'enabled', GlobalVariable.intWaitTime)

println("PhoneNumber Field is Editable"+phoneNumberIsEditable)

Mobile.tapAndHold(findTestObject('Object Repository/C2SC_MyTeamPage/input_PhoneNumber'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.clearText(findTestObject('Object Repository/C2SC_MyTeamPage/input_PhoneNumber'), GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_MyTeamPage/input_PhoneNumber'),'1895631236', GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/btn_Save'), GlobalVariable.intWaitTime)

//delete added preferred service

Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_MyTeamPage/img_verifyTitle'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/txt_TitleClosing'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_MyTeamPage/btn_Delete'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/btn_Delete'),GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_MyTeamPage/btn_DeleteServiceProvider'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_MyTeamPage/btn_Cancel'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/btn_DeleteServiceProvider'), GlobalVariable.intWaitTime)



Mobile.closeApplication()