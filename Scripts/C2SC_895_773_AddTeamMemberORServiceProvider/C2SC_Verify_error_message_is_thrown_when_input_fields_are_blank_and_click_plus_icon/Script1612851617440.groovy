import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_HomePageFlow'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('C2SC_AddTeamMemberScreen/tab_MyTeam'),GlobalVariable.intWaitTime)

Mobile.verifyElementText(findTestObject('C2SC_AddTeamMemberScreen/txt-MyTeam'),'My Team')

Mobile.tap(findTestObject('C2SC_AddTeamMemberScreen/btn_AddTeamMemberIcon'),GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_AddTeamMemberScreen/icon_closeTeamMember'),GlobalVariable.intWaitTime)

Mobile.verifyElementText(findTestObject('C2SC_AddTeamMemberScreen/txt-MyTeam'),'My Team')

Mobile.tap(findTestObject('C2SC_AddTeamMemberScreen/btn_AddTeamMemberIcon'),GlobalVariable.intWaitTime)

Mobile.verifyElementText(findTestObject('C2SC_AddTeamMemberScreen/txt_AddTeamMemberHeader'), 'Who would you like to add?')

Mobile.tap(findTestObject('C2SC_AddTeamMemberScreen/icon_PlusIconTeamMember'),GlobalVariable.intWaitTime)

Mobile.verifyElementText(findTestObject('C2SC_AddTeamMemberScreen/txt_validFirstName'), 'Please enter a valid first name')

Mobile.verifyElementText(findTestObject('C2SC_AddTeamMemberScreen/txt_validLastName'), 'Please enter a valid last name')

Mobile.verifyElementText(findTestObject('C2SC_AddTeamMemberScreen/txt_ validEmailID'), 'Please enter a valid Email ID')

Mobile.verifyElementText(findTestObject('C2SC_AddTeamMemberScreen/txt_validPhoneNumber'), 'Please enter a valid 10 digit phone number')

Mobile.closeApplication()

