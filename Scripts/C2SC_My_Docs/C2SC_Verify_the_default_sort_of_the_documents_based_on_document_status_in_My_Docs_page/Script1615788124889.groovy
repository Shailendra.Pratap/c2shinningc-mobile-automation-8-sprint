import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_MyProfilePage/tab_MyDocs'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MyProfilePage/tab_MyDocs'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('Object Repository/C2CS_MyDoc/txt_Contract'), GlobalVariable.intWaitTime)

Mobile.getText(findTestObject('Object Repository/C2CS_MyDoc/txt_Contract'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('Object Repository/C2CS_MyDoc/txt_LoanEstimate'), GlobalVariable.intWaitTime)

Mobile.getText(findTestObject('Object Repository/C2CS_MyDoc/txt_LoanEstimate'), GlobalVariable.intWaitTime)

Mobile.getText(findTestObject('Object Repository/C2CS_MyDoc/txt_Inspection'), GlobalVariable.intWaitTime)

Mobile.getText(findTestObject('Object Repository/C2CS_MyDoc/txt_Appraisal'), GlobalVariable.intWaitTime)

Mobile.getText(findTestObject('Object Repository/C2CS_MyDoc/txt_ WireInstructions'), GlobalVariable.intWaitTime)

Mobile.getText(findTestObject('Object Repository/C2CS_MyDoc/txt_ClosingDisclosure'), GlobalVariable.intWaitTime)

Mobile.scrollToText('Others', FailureHandling.STOP_ON_FAILURE)

Mobile.getText(findTestObject('Object Repository/C2CS_MyDoc/txt_Others'), GlobalVariable.intWaitTime)

Mobile.closeApplication()

