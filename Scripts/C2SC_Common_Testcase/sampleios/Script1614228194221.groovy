import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('bs://3ca43632f688a98809bf4bd4225de0b957db8183', true)

Mobile.tap(findTestObject('Object Repository/sample_ios/XCUIElementTypeButton - Ellipse 19'), 0)

Mobile.setText(findTestObject('Object Repository/sample_ios/XCUIElementTypeTextField - AuthCode_txt_Mobilenumber'), '1234512345', 
    0)

Mobile.tap(findTestObject('Object Repository/sample_ios/XCUIElementTypeButton - Done'), 0)

Mobile.setText(findTestObject('Object Repository/sample_ios/XCUIElementTypeTextField - AuthCode_txt_code1'), '1', 0)

Mobile.setText(findTestObject('Object Repository/sample_ios/XCUIElementTypeTextField - AuthCode_txt_code2'), '2', 0)

Mobile.setText(findTestObject('Object Repository/sample_ios/XCUIElementTypeTextField - AuthCode_txt_code3'), '3', 0)

Mobile.setText(findTestObject('Object Repository/sample_ios/XCUIElementTypeTextField - AuthCode_txt_code4'), '4', 0)

Mobile.tap(findTestObject('Object Repository/sample_ios/XCUIElementTypeStaticText - Submit Code'), 0)

Mobile.tap(findTestObject('Object Repository/sample_ios/XCUIElementTypeOther'), 0)

Mobile.tap(findTestObject('Object Repository/sample_ios/XCUIElementTypeButton - agentSelection_btn_next'), 0)

Mobile.scrollToText('Street')

Mobile.scrollToText('You are Buying')

Mobile.getText(findTestObject('Object Repository/sample_ios/XCUIElementTypeStaticText - You are Buying'), 0)

Mobile.scrollToText('You are Buying')

Mobile.scrollToText('Continue')

Mobile.scrollToText('Set up Profile')

Mobile.tap(findTestObject('Object Repository/sample_ios/XCUIElementTypeStaticText - Set up Profile'), 0)

Mobile.tap(findTestObject('Object Repository/sample_ios/XCUIElementTypeStaticText - Im a first-time home buyer'), 0)

Mobile.tap(findTestObject('Object Repository/sample_ios/XCUIElementTypeButton - tellus__btn_NextButton'), 0)

Mobile.tap(findTestObject('Object Repository/sample_ios/XCUIElementTypeButton - My Profile'), 0)

Mobile.tap(findTestObject('Object Repository/sample_ios/XCUIElementTypeButton - myprofile_btn_undercontract'), 0)

Mobile.tap(findTestObject('Object Repository/sample_ios/XCUIElementTypeStaticText - Mansion'), 0)

Mobile.getText(findTestObject('Object Repository/sample_ios/XCUIElementTypeStaticText - Insights'), 0)

Mobile.getText(findTestObject('Object Repository/sample_ios/XCUIElementTypeStaticText - List Price'), 0)

Mobile.getText(findTestObject('Object Repository/sample_ios/XCUIElementTypeStaticText - Est.Monthly'), 0)

Mobile.getText(findTestObject('Object Repository/sample_ios/XCUIElementTypeStaticText - propertyDetails_lbl_downpayment'), 
    0)

Mobile.getText(findTestObject('Object Repository/sample_ios/XCUIElementTypeStaticText - Est. Closing Costs'), 0)

Mobile.getText(findTestObject('Object Repository/sample_ios/XCUIElementTypeStaticText - Mortgage Rate  Term'), 0)

Mobile.tap(findTestObject('Object Repository/sample_ios/XCUIElementTypeButton - Path 25'), 0)

Mobile.getText(findTestObject('Object Repository/sample_ios/XCUIElementTypeStaticText - myprofile_lbl_fullname'), 0)

Mobile.getText(findTestObject('Object Repository/sample_ios/XCUIElementTypeStaticText - myprofile_lbl_email'), 0)

Mobile.getText(findTestObject('Object Repository/sample_ios/XCUIElementTypeStaticText - myprofile_lbl_buyerexp'), 0)

Mobile.getText(findTestObject('Object Repository/sample_ios/XCUIElementTypeStaticText - Terms of Use'), 0)

Mobile.getText(findTestObject('Object Repository/sample_ios/XCUIElementTypeStaticText - Privacy Policy'), 0)

Mobile.getText(findTestObject('Object Repository/sample_ios/XCUIElementTypeStaticText - Logout'), 0)

Mobile.getText(findTestObject('Object Repository/sample_ios/XCUIElementTypeStaticText - Create Team'), 0)

Mobile.closeApplication()

