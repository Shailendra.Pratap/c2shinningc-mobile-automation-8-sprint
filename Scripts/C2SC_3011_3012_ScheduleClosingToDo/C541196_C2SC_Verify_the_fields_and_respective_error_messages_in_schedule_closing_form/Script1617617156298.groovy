import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('C2SC_443_2855_ToDoTitleAnd Description/C2SC_Verify_Schedule_Closing(Calender) _title_and _description'), [('requestBodyPOST') : requestBodyPOST],
	FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_TodoScreen/txt_Schedule closing'), GlobalVariable.intWaitTime)
Mobile.tap(findTestObject('Object Repository/C2SC_TodoScreen/txt_Schedule closing'), GlobalVariable.intWaitTime)

//verifying fields
Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_TodoScreen/input_Schedule_txt_duedate'), GlobalVariable.intWaitTime)
Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_TodoScreen/input_Schedule_txt_time'), GlobalVariable.intWaitTime)
Mobile.tap(findTestObject('Object Repository/C2SC_TodoScreen/btn_Add'), GlobalVariable.intWaitTime)
Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_TodoScreen/txt_Schedule_err_validTime'),  GlobalVariable.intWaitTime)
Mobile.closeApplication()