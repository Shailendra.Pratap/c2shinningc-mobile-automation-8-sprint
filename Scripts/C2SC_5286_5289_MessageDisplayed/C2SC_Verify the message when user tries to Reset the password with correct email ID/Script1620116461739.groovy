import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication(GlobalVariable.sAppID, true)

WebUI.delay(10)

String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals('iOS')) {
    Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)
}

Mobile.verifyElementExist(findTestObject('C2SC_LoginScreen/lnk_Login'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_LoginScreen/lnk_Login'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_ForgotPassword/lnk_forgotpassword'), GlobalVariable.intWaitTime)

Mobile.getText(findTestObject('Object Repository/C2SC_ForgotPassword/txt_ForgotPassword'), GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_ForgotPassword/input_Emailid'), GlobalVariable.intWaitTime, 0)

Mobile.setText(findTestObject('Object Repository/C2SC_ForgotPassword/input_Emailid'), 'rose@email3.com', GlobalVariable.intWaitTime)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Object Repository/C2SC_ForgotPassword/lnk_ResetPassword'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ForgotPassword/txt_Message'), GlobalVariable.intWaitTime)

Mobile.verifyElementText(findTestObject('C2SC_ForgotPassword/txt_Message'), 'If the email address you entered matches our records, a link for resetting your password will be sent via email to the supplied address. If you do not receive a link, please contact technical support on (800) 873–811')

Mobile.tap(findTestObject('C2SC_ForgotPassword/btn_Ok'), GlobalVariable.intWaitTime)

Mobile.delay(3)

Mobile.verifyElementExist(findTestObject('C2SC_ChangePasswordFromMyProfileScreen/img_coldwell'), GlobalVariable.intWaitTime)

Mobile.closeApplication()

