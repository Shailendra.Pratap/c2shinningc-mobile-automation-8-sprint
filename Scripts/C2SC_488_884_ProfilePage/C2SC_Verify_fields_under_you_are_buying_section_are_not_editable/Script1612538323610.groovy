import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_Login'), [('lstReturned') : [:], ('requestBodyPOST') : '{\r\n  "sourceApp": "MyDeals",\r\n  "transactionId": "Tomtom11",\r\n  "officeId" :"2",\r\n    \r\n  "contractDate": "2021-03-20T11:23:48Z",\r\n  "estimatedClosingDate":"2021-08-04T11:23:48Z",\r\n  "agent": [\r\n    {\r\n      "agentOktaId": "00upmrou2f4RF7CrT0h7",\r\n      "agentTridentId": "TestAgent2OktaId"\r\n    }\r\n  ],\r\n  "property": {\r\n    "mlsID": "123",\r\n    "addressLine1": "street1",\r\n    "addressLine2": "lane 4",\r\n    "city": "seattle",\r\n    "state": "Texas",\r\n    "zip": "98101"\r\n  },\r\n  "consumer": [\r\n    {\r\n        \r\n      "firstName": "geetanjali",\r\n      "lastName": "gavi",\r\n      "emailId": "test@test.com",\r\n      "phoneNo": "9898989898",\r\n      "country": "US"\r\n    }\r\n  ]\r\n}'], 
    FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

Mobile.scrollToText('Set up Profile')

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/txt_Street'), GlobalVariable.intWaitTime)

Mobile.waitForElementAttributeValue(findTestObject('C2SC_ProfileSetup/txt_Street'), 'enabled', 'false', GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/txt_City'), GlobalVariable.intWaitTime)

Mobile.verifyElementAttributeValue(findTestObject('C2SC_ProfileSetup/txt_City'), 'enabled', 'true', GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/txt_State'), GlobalVariable.intWaitTime)

Mobile.waitForElementAttributeValue(findTestObject('C2SC_ProfileSetup/txt_State'), 'enabled', 'false', GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/txt_ZipCode'), GlobalVariable.intWaitTime)

Mobile.waitForElementAttributeValue(findTestObject('C2SC_ProfileSetup/txt_ZipCode'), 'enabled', 'false', GlobalVariable.intWaitTime)

/*
for(int i=0; i<=3; i++)
{
try{
	if(i==0){
		Mobile.setText(findTestObject('C2SC_ProfilePage/txt_Street'), 'abc', GlobalVariable.intWaitTime)
		KeywordUtil.markFailed("Field is Editable")
	}
	if(i==1){
		Mobile.setText(findTestObject('C2SC_ProfilePage/txt_City'), 'abc', GlobalVariable.intWaitTime)
		KeywordUtil.markFailed("Field is Editable")
	}
	if(i==2){
		Mobile.setText(findTestObject('C2SC_ProfilePage/txt_State'), 'abc', GlobalVariable.intWaitTime)
		KeywordUtil.markFailed("Field is Editable")
	}
	if(i==3){
		Mobile.setText(findTestObject('C2SC_ProfilePage/txt_Zip'), 'abc', GlobalVariable.intWaitTime)
		KeywordUtil.markFailed("Field is Editable")
	}
		
	}catch(Exception e)
	{
		KeywordUtil.markPassed("Field is not Editable")
	}
}*/
//Mobile.verifyElementAttributeValue(findTestObject('C2SC_ProfilePage/txt_Street'), 'focusable', 'false', GlobalVariable.intWaitTime)
//Mobile.verifyElementAttributeValue(findTestObject('C2SC_ProfilePage/txt_City'), 'focusable', 'false', GlobalVariable.intWaitTime)
//Mobile.verifyElementAttributeValue(findTestObject('C2SC_ProfilePage/txt_State'), 'focusable', 'false', GlobalVariable.intWaitTime)
//Mobile.verifyElementAttributeValue(findTestObject('C2SC_ProfilePage/txt_Zip'), 'focusable', 'false', GlobalVariable.intWaitTime)
Mobile.closeApplication()

