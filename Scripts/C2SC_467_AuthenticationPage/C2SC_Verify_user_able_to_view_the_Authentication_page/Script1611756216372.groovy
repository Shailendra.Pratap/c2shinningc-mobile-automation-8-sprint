import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication(GlobalVariable.sAppID, true)

WebUI.delay(10)

String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals("iOS")) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime,FailureHandling.OPTIONAL)
}


//Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_NextArrow'), GlobalVariable.intWaitTime)
//
//Mobile.verifyElementExist(findTestObject('C2SC_CommonObject/btn_NextArrow'), GlobalVariable.intWaitTime)
//
//Mobile.tap(findTestObject('C2SC_CommonObject/btn_NextArrow'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/input_Mobile'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_AuthenticationPage/txt_Mobile_Number'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_AuthenticationPage/txt_Invitation_Code'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_CommonObject/btn_Submit'), GlobalVariable.intWaitTime)

Mobile.closeApplication()

