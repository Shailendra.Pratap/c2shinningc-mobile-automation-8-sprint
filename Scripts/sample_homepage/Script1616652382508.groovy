import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('bs://53673d32322bbbec940f19abe73f7426cd9cb9c5', true)

Mobile.tap(findTestObject('Object Repository/sample_homepage/android.widget.TextView - Log In'), 0)

Mobile.setText(findTestObject('Object Repository/sample_homepage/android.widget.EditText'), 'c2sc2869@test.com', 0)

Mobile.setText(findTestObject('Object Repository/sample_homepage/android.widget.EditText (1)'), 'Mindtree@1234', 0)

Mobile.tap(findTestObject('Object Repository/sample_homepage/android.widget.Button - Submit'), 0)

Mobile.getText(findTestObject('sample_homepage/txt_ScheduleInspection'), 0)

Mobile.getText(findTestObject('sample_homepage/tab_Submit_your_initial_earnest_money'), 
    0)

Mobile.getText(findTestObject('Object Repository/sample_homepage/android.widget.TextView - Due in 6 days'), 0)

Mobile.tap(findTestObject('sample_homepage/tab_ScheduleInspection'), 0)

Mobile.getText(findTestObject('Object Repository/sample_homepage/android.widget.TextView - Schedule Inspection (1)'), 0)

Mobile.setText(findTestObject('sample_homepage/input_Company'), 'test', 0)

Mobile.tap(findTestObject('sample_homepage/input_Duedate'), 0)

Mobile.tap(findTestObject('Object Repository/sample_homepage/android.view.View - 31'), 0)

Mobile.tap(findTestObject('Object Repository/sample_homepage/android.widget.Button - OK'), 0)

Mobile.tap(findTestObject('sample_homepage/input_Time'), 0)

Mobile.tap(findTestObject('Object Repository/sample_homepage/android.widget.RadialTimePickerViewRadialPickerTouchHelper'), 
    0)

Mobile.tap(findTestObject('Object Repository/sample_homepage/android.widget.RadialTimePickerViewRadialPickerTouchHelper (1)'), 
    0)

Mobile.tap(findTestObject('Object Repository/sample_homepage/android.widget.Button - OK (1)'), 0)

Mobile.tap(findTestObject('Object Repository/sample_homepage/android.view.ViewGroup'), 0)

Mobile.tap(findTestObject('sample_homepage/btn_Add'), 0)

Mobile.tap(findTestObject('Object Repository/sample_homepage/android.widget.TextView - Timeline'), 0)

Mobile.getText(findTestObject('Object Repository/sample_homepage/android.widget.TextView - Schedule Inspection (2)'), 0)

Mobile.closeApplication()

