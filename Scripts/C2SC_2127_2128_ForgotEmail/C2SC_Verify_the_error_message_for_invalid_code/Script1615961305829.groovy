import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication(GlobalVariable.sAppID, true)
WebUI.delay(10)

String AndroidDevice = GlobalVariable.sDeviceAndroid

if(Mobile.verifyElementExist(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
}

Mobile.verifyElementExist(findTestObject('C2SC_LoginScreen/lnk_Login'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_LoginScreen/lnk_Login'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_ForgotEmail/lnk_forgotEmail'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_ForgotEmail/lnk_forgotEmail'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_ForgotEmail/txt_HeaderForgotEmail'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_ForgotEmail/input_MobileNumber'), GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_ForgotEmail/input_MobileNumber'), '7406795028', GlobalVariable.intWaitTime)

if(Mobile.verifyElementExist(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_ForgotEmail/btn_RetrieveEmail'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_ForgotEmail/btn_RetrieveEmail'), GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('Object Repository/C2SC_ForgotEmail/input_enterOtp1'), 3, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_ForgotEmail/input_enterOtp1'), '0' , GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_ForgotEmail/input_enterOtp2'), '0', GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_ForgotEmail/input_enterOtp3'),'0', GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_ForgotEmail/input_enterOtp4'),'0', GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_ForgotEmail/btn_SubmitCode'), GlobalVariable.intWaitTime)

if(Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_ForgotEmail/btn_Ok'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
	Mobile.tap(findTestObject('Object Repository/C2SC_ForgotEmail/btn_Ok'), GlobalVariable.intWaitTime)
}

Mobile.verifyElementText(findTestObject('Object Repository/C2SC_ForgotEmail/txt_InvalidCodeError'), 'Please enter a valid code',FailureHandling.OPTIONAL)

Mobile.closeApplication()

