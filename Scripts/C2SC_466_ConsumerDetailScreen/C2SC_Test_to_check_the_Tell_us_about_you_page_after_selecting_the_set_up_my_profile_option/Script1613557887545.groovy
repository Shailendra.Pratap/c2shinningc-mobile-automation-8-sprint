import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_Login'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

try {
    String newpassword = Mobile.getText(findTestObject('C2SC_CommonObject/txt_SetNewPassword'), GlobalVariable.intWaitTime)

    if (!(newpassword.equals(null))) {
        Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/input_NewPassword'), GlobalVariable.intWaitTime)

        Mobile.setText(findTestObject('C2SC_CommonObject/input_NewPassword'), 'Mindtree@1234', GlobalVariable.intWaitTime)

        Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), GlobalVariable.intWaitTime)

        Mobile.setText(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), 'Mindtree@1234', GlobalVariable.intWaitTime)

        Mobile.tap(findTestObject('C2SC_CommonObject/btn_Continue'), GlobalVariable.intWaitTime)
    }
}
catch (Exception e) {
} 

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/txt_FirstName'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_ProfileSetup/txt_FirstName'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/txt_LastName'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_ProfileSetup/txt_LastName'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/txt_Email'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_ProfileSetup/txt_Email'), GlobalVariable.intWaitTime)

Mobile.scrollToText('Set up Profile')

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/txt_Phone'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_ProfileSetup/txt_Phone'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/txt_Street'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_ProfileSetup/txt_Street'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/txt_City'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_ProfileSetup/txt_City'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/txt_State'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_ProfileSetup/txt_State'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/txt_ZipCode'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_ProfileSetup/txt_ZipCode'), GlobalVariable.intWaitTime)

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_SetUpProfile'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_SetUpProfile'), GlobalVariable.intWaitTime)

def tellUsHeader = Mobile.getText(findTestObject('C2SC_TellUsLittleAboutYouScreen/txt_TellUsALittleAboutYou'), GlobalVariable.intWaitTime)

Mobile.verifyEqual(tellUsHeader, 'Tell us a little about you')

Mobile.closeApplication()

