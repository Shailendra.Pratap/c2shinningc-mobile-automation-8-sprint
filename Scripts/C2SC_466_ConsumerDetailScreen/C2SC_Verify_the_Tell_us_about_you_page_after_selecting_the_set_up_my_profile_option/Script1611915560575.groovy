import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_Login'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

//Mobile.verifyElementExist(findTestObject('C2SC_466_ProfileValidationScreen/txt_WelcomeText'), GlobalVariable.intWaitTime)

def validateInfo = Mobile.getText(findTestObject('C2SC_466_ProfileValidationScreen/txt_ValidateInfo'), GlobalVariable.intWaitTime)

Mobile.verifyEqual(validateInfo, 'Let’s validate your information so we can get started')

def buyerInfoTitle = Mobile.getText(findTestObject('C2SC_466_ProfileValidationScreen/txt_buyerInfo'), GlobalVariable.intWaitTime)

Mobile.verifyEqual(buyerInfoTitle, 'Your Information')

Mobile.verifyElementExist(findTestObject('C2SC_466_ProfileValidationScreen/input_FirstName'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_466_ProfileValidationScreen/input_LastName'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_466_ProfileValidationScreen/input_Email'), GlobalVariable.intWaitTime)

Mobile.scrollToText('zip')

Mobile.verifyElementExist(findTestObject('C2SC_466_ProfileValidationScreen/input_Phone'), GlobalVariable.intWaitTime)

def propertAddressTitle = Mobile.getText(findTestObject('C2SC_466_ProfileValidationScreen/txt_buyerProperty'), GlobalVariable.intWaitTime)

Mobile.verifyEqual(propertAddressTitle, 'You are Buying')

Mobile.verifyElementExist(findTestObject('C2SC_466_ProfileValidationScreen/input_Street'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_466_ProfileValidationScreen/input_City'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_466_ProfileValidationScreen/input_State'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_466_ProfileValidationScreen/input_ZipCode'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_TellUsLittleAboutYouScreen/btn_Set up Profile'), GlobalVariable.intWaitTime)

def tellUsHeader = Mobile.getText(findTestObject('C2SC_TellUsLittleAboutYouScreen/txt_TellUsALittleAboutYou'), GlobalVariable.intWaitTime)

Mobile.verifyEqual(tellUsHeader, 'Tell us a little about you')

Mobile.closeApplication()

