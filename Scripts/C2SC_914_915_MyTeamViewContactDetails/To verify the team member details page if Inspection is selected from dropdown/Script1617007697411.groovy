import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebElement

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable
import io.appium.java_client.MobileDriver
import io.appium.java_client.MobileElement


WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_HomePage/txt_My Team'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_HomePage/txt_My Team'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_HomePage/txt_My Team'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_AddTeamMemberScreen/btn_AddTeamMemberIcon'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_AddTeamMemberScreen/txt_AddTeamMemberHeader'), GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('Object Repository/C2SC_MyTeamPage/input_ContactName'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_MyTeamPage/input_ContactName'), 'Ztin', GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime,FailureHandling.CONTINUE_ON_FAILURE)

Mobile.delay(GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('Object Repository/C2SC_MyTeamPage/input_Email'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

String email=CustomKeywords.'com.Utilities.Utility.getRandomEmail'()

String getRandomEmailId=email.toLowerCase()

Mobile.setText(findTestObject('C2SC_AddTeamMemberScreen/input_email'),getRandomEmailId, GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime,FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tapAndHold(findTestObject('Object Repository/C2SC_MyTeamPage/input_PhoneNumber'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_MyTeamPage/input_PhoneNumber'), '1234567890', GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime,FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('C2SC_AddTeamMemberScreen/icon_PlusIconTeamMember'), GlobalVariable.intWaitTime)

Mobile.scrollToText('Ztin',FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('C2SC_AddTeamMemberScreen/txt_MyTeamName'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_MyTeamPage/btn_Delete'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_MyTeamPage/btn_Delete'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_MyTeamPage/btn_Edit'),GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/btn_Delete'),GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_MyTeamPage/txt_AreYouSureYouWantToDiscontinue'),GlobalVariable.intWaitTime)

String deletePopUpText=Mobile.getText(findTestObject('Object Repository/C2SC_MyTeamPage/txt_AreYouSureYouWantToDiscontinue'),GlobalVariable.intWaitTime)

println(deletePopUpText)

//delete
Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_MyTeamPage/btn_DeleteServiceProvider'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_MyTeamPage/btn_Cancel'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/btn_DeleteServiceProvider'), GlobalVariable.intWaitTime)

Mobile.scrollToText('Ztin',FailureHandling.OPTIONAL)

Mobile.closeApplication()



