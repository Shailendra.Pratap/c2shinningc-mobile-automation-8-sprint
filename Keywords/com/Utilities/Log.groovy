package com.Utilities;

import java.io.IOException;

import javax.swing.JLabel;
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.util.KeywordUtil;


public class Log {

	KeywordLogger log = new KeywordLogger();

	//Logging information
	public void info(String message) throws IOException {

		log.logInfo(message);
		KeywordUtil.logInfo(message);
	}
	//Logging warning
	public void warning(String message) throws Exception {

		log.logWarning(message);
		KeywordUtil.markWarning(message);


	}

	//Logging error
	public void error(String message) throws Exception {

		log.logError(message);
		KeywordUtil.markErrorAndStop(message);



	}

	//Logging fatal error
	public void fatal(String message) throws Exception {

		log.logError(message);
		KeywordUtil.markFailedAndStop(message);


	}
	//Logging pass
	public void pass(String message) throws Exception {

		log.logInfo(message);
		KeywordUtil.markPassed(message);



	}

	//Logging fail
	public void fail(String message) throws Exception {

		log.logError(message);
		KeywordUtil.markPassed(message);


	}





}
