<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Site_CantBeReached</name>
   <tag></tag>
   <elementGuidId>dd460f7e-c3da-414f-8fdd-1834740ac34d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[text()=&quot;This site can’t be reached&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
