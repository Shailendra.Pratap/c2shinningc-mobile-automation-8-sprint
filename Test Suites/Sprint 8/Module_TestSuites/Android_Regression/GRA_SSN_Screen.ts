<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>GRA_SSN_Screen</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>36b1008e-8d93-4d0c-b285-a34fc1188ce4</testSuiteGuid>
   <testCaseLink>
      <guid>66eb1044-61cb-486f-adab-cab8f7a85323</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC-4924_GRA/C577704_C2SC_Verify_that_check_box_is_available_in_SSN_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>21aeacd7-6f39-44fa-9983-aaf689ad1d8c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC-4924_GRA/C577705_C2SC_Verify_the_description_for_the_check_box_in_SSN_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d23fcfdb-730d-43cc-9b96-582d3154ff6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC-4924_GRA/C577706_C2SC_Verify_the_Save_button_is_disabled_before_selecting_check_box</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9603112e-bfe6-4561-8d8d-68ff868f2429</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC-4924_GRA/C577707_C2SC_Verify_the_Save_button_is_enabled_after_selecting_check_box</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>71b9f407-185a-43f2-a6c6-42d809e059a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC-4924_GRA/C577708_C2SC_Verify_that_GRA_is_added_for_successfully_for_Mortgage</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
