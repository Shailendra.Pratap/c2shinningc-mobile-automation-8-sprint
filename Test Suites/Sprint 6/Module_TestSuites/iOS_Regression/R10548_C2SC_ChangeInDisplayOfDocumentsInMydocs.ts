<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10548_C2SC_ChangeInDisplayOfDocumentsInMydocs</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>4958e028-5718-40f7-b3f8-e113343f8dca</testSuiteGuid>
   <testCaseLink>
      <guid>cd34ab7d-f5e7-4ad2-b2a6-3344923b857c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2922_2923_ChangeInDisplayOfDocumentsInMydocs/C2SC_Verify _the _document_ icon(pdf) in _an_ empty_ bucket(Category)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd84f338-8e90-459f-afd8-0d7ae19d08ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2922_2923_ChangeInDisplayOfDocumentsInMydocs/C2SC_Verify _the_ appearance _of _Date_ under _particular_ document _only_ when there is a document in place</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b50a0d16-60df-4918-8beb-9c4ee979844b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2922_2923_ChangeInDisplayOfDocumentsInMydocs/C2SC_Verify_the_sections_ under_My Docs</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
