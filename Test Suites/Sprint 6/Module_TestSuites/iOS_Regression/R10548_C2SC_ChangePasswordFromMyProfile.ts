<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10548_C2SC_ChangePasswordFromMyProfile</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>1f251f3e-a2f6-410e-b4c6-abdb328dcdbd</testSuiteGuid>
   <testCaseLink>
      <guid>88889fad-824e-46bd-9a58-e6ee7473fb9e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2120_2017_ChangePasswordFromMyProfileScreen/C2SC_Verify_change_password_in_my_profile_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7af37545-2e30-40ca-873e-6dac91a45859</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2120_2017_ChangePasswordFromMyProfileScreen/C2SC_Verify_condition_to_update_the_password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6fa592a1-0962-4073-b682-821f15ce3a52</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2120_2017_ChangePasswordFromMyProfileScreen/C2SC_Verify_that_password_updated_succesfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d3b2f40-6c79-4806-be8e-c33136b45b1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2120_2017_ChangePasswordFromMyProfileScreen/C2SC_Verify_the_error_message_for_all_mandatory_fields</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a699c879-b6c8-47ca-88b5-180ba3a572e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2120_2017_ChangePasswordFromMyProfileScreen/C2SC_Verify_the_password_is_encrypted</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
