<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_FeedbackScreen</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e9ca85c2-62b7-428c-a870-dedc8cd8bb63</testSuiteGuid>
   <testCaseLink>
      <guid>52eab9d2-ca20-4b33-b9d7-b311b108a3ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_FeedbackScreen/C2SC_Verify_consumer_is_able_to_view_and_enter_the_feedback_in_the_feedback_form-new_user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cd3673d4-f3b5-4f1c-be28-f3c62b486177</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_FeedbackScreen/C2SC_Verify_consumer_is_able_to_view_and_enter_the_feedback_in_the_feedback_form-Registered_user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7837ce9b-fb5c-4cfe-a808-c8e892f19d16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_FeedbackScreen/C2SC_Verify_display_of_success_message_on_successful_submission_of_the_feedback</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
