<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10548_ViewAndManageNotifications</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>405654fd-31b9-4908-815b-566810f79bbe</testSuiteGuid>
   <testCaseLink>
      <guid>9878ac64-50a1-4ebd-810b-eb671c6b11af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1422_262_ViewAndManageNotifications/Verify the presence of Clear All option when at least one notification is displayed under notifications section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>52053880-efd3-4dee-9193-fc37c1cbf077</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1422_262_ViewAndManageNotifications/Verify_if_user_is_able_to_view_the_To_do_related_notifications_displayed_on_Notifications_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8bd0d8e-1558-47b4-a36b-7808508c50f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1422_262_ViewAndManageNotifications/Verify_the_content_of_notifications_in_notification_tab</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
