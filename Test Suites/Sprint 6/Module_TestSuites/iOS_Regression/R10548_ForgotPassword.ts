<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10548_ForgotPassword</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>2572ea8a-d759-4c33-b71a-bc256c21ff9a</testSuiteGuid>
   <testCaseLink>
      <guid>d019a3dc-da75-404e-800a-aff024d76114</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2130_2131_ForgotPassword/Verify_the_forgot_password_screen_on_entering_registered_email_id</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
