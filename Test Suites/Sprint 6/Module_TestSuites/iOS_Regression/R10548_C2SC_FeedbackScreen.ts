<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10548_C2SC_FeedbackScreen</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>f9cd9b76-6a10-4d47-85dc-62f0e4a51045</testSuiteGuid>
   <testCaseLink>
      <guid>5060eadc-5dc3-45b8-b0f6-cc677d900db6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_FeedbackScreen/C2SC_Verify_consumer_is_able_to_view_and_enter_the_feedback_in_the_feedback_form-new_user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5def4a7-c543-4b20-89d3-0475e1d329e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_FeedbackScreen/C2SC_Verify_consumer_is_able_to_view_and_enter_the_feedback_in_the_feedback_form-Registered_user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83629288-8219-45fd-83e4-d0534f8f33d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_FeedbackScreen/C2SC_Verify_display_of_success_message_on_successful_submission_of_the_feedback</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
