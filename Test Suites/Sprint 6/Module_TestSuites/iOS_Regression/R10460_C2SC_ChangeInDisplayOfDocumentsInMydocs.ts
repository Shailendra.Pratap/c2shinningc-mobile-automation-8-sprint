<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_ChangeInDisplayOfDocumentsInMydocs</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>053bb1c5-ca72-41a5-8181-02381e8f6144</testSuiteGuid>
   <testCaseLink>
      <guid>67b78e9e-d5d7-486a-b3ad-f3ad9c352f1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2922_2923_ChangeInDisplayOfDocumentsInMydocs/C2SC_Verify _the _document_ icon(pdf) in _an_ empty_ bucket(Category)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e01b034-fafd-4d76-b728-5b9d1cd84211</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2922_2923_ChangeInDisplayOfDocumentsInMydocs/C2SC_Verify _the_ appearance _of _Date_ under _particular_ document _only_ when there is a document in place</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>98563e9f-9c05-4ee9-9868-4a496c8c551a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2922_2923_ChangeInDisplayOfDocumentsInMydocs/C2SC_Verify_the_sections_ under_My Docs</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
