<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10548_SettingsScreen</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>2931adce-9207-4306-b9dc-05e95a4e87f7</testSuiteGuid>
   <testCaseLink>
      <guid>c8c80e94-cb12-4a8b-b329-bd8e11a96d67</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2015_2125_SettingsSection/C2SC_Verify_About_app_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>427377c2-df0d-481e-b6cc-9cfcf50cd87d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2015_2125_SettingsSection/C2SC_Verify_Privacy_policy_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>14702486-b0ea-4662-894e-815595179523</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2015_2125_SettingsSection/C2SC_Verify_Terms_of_Use_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49114aeb-46ce-48b1-861d-17a8e8855ebd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2015_2125_SettingsSection/C2SC_Verify_the_settings_options_under_profile_details_in _My Profile_screen</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
