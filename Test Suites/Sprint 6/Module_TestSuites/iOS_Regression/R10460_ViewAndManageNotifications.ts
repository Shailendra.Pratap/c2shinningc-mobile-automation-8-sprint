<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_ViewAndManageNotifications</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>aebc6657-4cf6-4453-ba3e-885d73598a12</testSuiteGuid>
   <testCaseLink>
      <guid>1520af4a-8d04-441e-b2fb-1e4ee52b2b82</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1422_262_ViewAndManageNotifications/Verify the presence of Clear All option when at least one notification is displayed under notifications section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0785a8b2-ecdc-47ac-91b7-acbadcb4b663</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1422_262_ViewAndManageNotifications/Verify_if_user_is_able_to_view_the_To_do_related_notifications_displayed_on_Notifications_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>faf64267-034c-419f-aaeb-6ffa395abc0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1422_262_ViewAndManageNotifications/Verify_the_content_of_notifications_in_notification_tab</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
