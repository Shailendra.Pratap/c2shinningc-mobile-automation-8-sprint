<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10548_Homepage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>8ad0c23a-dc8b-4b35-b373-a1ed9243e2d9</testSuiteGuid>
   <testCaseLink>
      <guid>c5443543-34c6-4893-875f-e4d8ccec180a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_Mark_as_done_in_the_to_do_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5866728b-5db9-422f-8539-e0fc776abccc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_no_document_is_available_for_the_to_do_task_when_a_no_service_provider_is_selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>825019a6-69a3-4e1a-bf72-987912795cd2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_no_document_is_available_for_the_to_do_task_when_a_recommended_service_provider_is_selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>30bd3005-852b-4a0b-b3b7-e31ca11cb3e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_no_document_is_available_for_the_to_do_task_when_a_self_preferred_service_provider_is_selected</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
