<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_ForgotPassword</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>28f3bc4d-d7aa-4201-af4a-f0570a44e643</testSuiteGuid>
   <testCaseLink>
      <guid>dd9ec75b-b41e-41b9-8351-6b8987a859ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2130_2131_ForgotPassword/Verify_the_forgot_password_screen_on_entering_registered_email_id</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
