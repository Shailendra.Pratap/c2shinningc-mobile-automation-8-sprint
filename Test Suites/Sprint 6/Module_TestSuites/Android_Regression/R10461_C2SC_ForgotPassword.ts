<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_ForgotPassword</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>723821bf-49d2-404d-a91a-4f9a668b18ce</testSuiteGuid>
   <testCaseLink>
      <guid>67e20357-ddba-4eb8-ba65-f90a22182898</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2130_2131_ForgotPassword/Verify_the_forgot_password_screen_on_entering_registered_email_id</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
