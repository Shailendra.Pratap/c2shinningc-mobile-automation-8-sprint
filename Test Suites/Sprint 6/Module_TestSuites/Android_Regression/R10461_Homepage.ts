<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_Homepage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>798796f9-ae39-4cda-8d2e-6552f730bf59</testSuiteGuid>
   <testCaseLink>
      <guid>17fa82ba-6a99-4989-be25-19d50e22dad2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_no_document_is_available_for_the_to_do_task_when_a_no_service_provider_is_selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96cf1b65-6c24-4351-9865-b957a2bd6141</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_no_document_is_available_for_the_to_do_task_when_a_recommended_service_provider_is_selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d5103a3-5203-4a2d-8eff-5fd60fba4faa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_no_document_is_available_for_the_to_do_task_when_a_self_preferred_service_provider_is_selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b7064d5f-87b9-45cb-9ce2-1fcf1bd1fd85</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_Mark_as_done_in_the_to_do_task</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
