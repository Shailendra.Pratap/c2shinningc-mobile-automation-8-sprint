<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_ViewAndManageNotifications</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b17ed5d7-afcf-4d07-909d-8e0b3ea15bda</testSuiteGuid>
   <testCaseLink>
      <guid>67edf273-900f-4df0-9139-b511bc205392</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1422_262_ViewAndManageNotifications/Verify the presence of Clear All option when at least one notification is displayed under notifications section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18ded9fb-c2e4-4df1-a255-c80d0deaf90c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1422_262_ViewAndManageNotifications/Verify_if_user_is_able_to_view_the_To_do_related_notifications_displayed_on_Notifications_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c9254f99-c63b-403c-974d-3bbbc333a11e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1422_262_ViewAndManageNotifications/Verify_the_content_of_notifications_in_notification_tab</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
