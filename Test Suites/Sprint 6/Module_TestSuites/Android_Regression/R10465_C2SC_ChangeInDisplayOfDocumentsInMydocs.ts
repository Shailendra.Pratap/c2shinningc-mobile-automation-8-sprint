<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10465_C2SC_ChangeInDisplayOfDocumentsInMydocs</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>643f6798-7891-4018-b07a-c7f8d1c2a65f</testSuiteGuid>
   <testCaseLink>
      <guid>9f878080-f49a-419c-97e0-64e08938b591</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2922_2923_ChangeInDisplayOfDocumentsInMydocs/C2SC_Verify _the _document_ icon(pdf) in _an_ empty_ bucket(Category)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a8f98ffe-28a2-4b16-a6b5-23a0e5aac843</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2922_2923_ChangeInDisplayOfDocumentsInMydocs/C2SC_Verify _the_ appearance _of _Date_ under _particular_ document _only_ when there is a document in place</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e279bdf-4e49-41a7-b758-29983aaa3b56</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2922_2923_ChangeInDisplayOfDocumentsInMydocs/C2SC_Verify_the_sections_ under_My Docs</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
