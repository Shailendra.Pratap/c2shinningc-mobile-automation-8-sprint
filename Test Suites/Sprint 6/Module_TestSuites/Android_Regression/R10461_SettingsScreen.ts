<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_SettingsScreen</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>76ab5fa8-da67-43f6-95e2-45826996f9ff</testSuiteGuid>
   <testCaseLink>
      <guid>92a2107a-a3a4-41e0-8035-07330b62823c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2015_2125_SettingsSection/C2SC_Verify_the_settings_options_under_profile_details_in _My Profile_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e089bb88-bccb-431c-8919-99f958d2cbc6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2015_2125_SettingsSection/C2SC_Verify_About_app_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2f27506-18b2-4763-ba55-f21a47779fa1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2015_2125_SettingsSection/C2SC_Verify_Terms_of_Use_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dc9558e6-b5ed-4102-83ec-599ab4764ff2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2015_2125_SettingsSection/C2SC_Verify_Privacy_policy_option</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
