<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteCollectionEntity>
   <description></description>
   <name>Regression_TestSuite_Android_GooglePixel4_QA</name>
   <tag></tag>
   <executionMode>PARALLEL</executionMode>
   <maxConcurrentInstances>8</maxConcurrentInstances>
   <testSuiteRunConfigurations>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 6/Module_TestSuites/Android_Regression/R10461_C2SC_FeedbackScreen</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 6/Module_TestSuites/Android_Regression/R10461_C2SC_ForgotPassword</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 6/Module_TestSuites/Android_Regression/R10461_C2SC_MyDocsScreen</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 6/Module_TestSuites/Android_Regression/R10461_SettingsScreen</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 6/Module_TestSuites/Android_Regression/R10465_C2SC_ChangeInDisplayOfDocumentsInMydocs</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 6/Module_TestSuites/Android_Regression/R10461_ViewAndManageNotifications</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 6/Module_TestSuites/Android_Regression/R10461_Homepage</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 6/Module_TestSuites/iOS_Regression/R10548_C2SC_ChangePasswordFromMyProfile</testSuiteEntity>
      </TestSuiteRunConfiguration>
   </testSuiteRunConfigurations>
</TestSuiteCollectionEntity>
