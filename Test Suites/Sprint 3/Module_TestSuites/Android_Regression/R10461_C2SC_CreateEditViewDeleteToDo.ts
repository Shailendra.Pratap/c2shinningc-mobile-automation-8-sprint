<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_CreateEditViewDeleteToDo</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e0a2038a-7241-4dba-ad6b-6e10dca94656</testSuiteGuid>
   <testCaseLink>
      <guid>45da967b-1bf1-4755-ba8c-113e1e558c7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1413_1415_CreateEditViewDeleteToDo/C2SC_To_validate_the_View_task_displayed_on_clicking_the_self_assigned_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a225255d-3f49-4a12-87bd-4ba5f6aef4d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1413_1415_CreateEditViewDeleteToDo/C2SC_To_verify_clicking_delete_option_for_a_self_assigned_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>edac64eb-7607-414d-ba71-6eb8695aa1a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1413_1415_CreateEditViewDeleteToDo/C2SC_To_verify_clicking_edit_option_for_a_self_assigned_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f98c3e14-fd1a-4bd8-a98b-c9c3ef4440d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1413_1415_CreateEditViewDeleteToDo/C2SC_To_verify_marking_a_self_assigned_task_as_done</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
