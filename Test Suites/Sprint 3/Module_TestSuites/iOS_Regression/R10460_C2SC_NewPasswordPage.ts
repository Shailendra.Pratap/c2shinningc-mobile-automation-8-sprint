<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_NewPasswordPage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>593463a6-317a-4c97-8034-11a99778bbe3</testSuiteGuid>
   <testCaseLink>
      <guid>b1f37e47-710b-4da7-a414-4857501de316</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1145_1144_NewPasswordPage/C2SC_Verify_display_of_error_message_when_confirm_password_do_not_match_with_new_password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7424aa72-e92a-459d-88f0-1dfe28360d07</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1145_1144_NewPasswordPage/C2SC_Verify_navigation_to_NewPassword_Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>91f2397c-ecbb-49f2-b244-e0f6daf9750a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1145_1144_NewPasswordPage/C2SC_Verify_the_contents_of_registration_page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
