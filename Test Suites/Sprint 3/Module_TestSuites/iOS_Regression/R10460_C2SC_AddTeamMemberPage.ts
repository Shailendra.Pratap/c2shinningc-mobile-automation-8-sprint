<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_AddTeamMemberPage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>744acda1-3ecc-473a-9bc3-753f1d44e560</testSuiteGuid>
   <testCaseLink>
      <guid>b119c63f-debf-402e-b6d1-04860ccfcfee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1420_1421_AddTeamMemberPage/C2SC_Verify_error_message_on_not_entering_phone_number</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f6c32cdf-9cd9-4372-aa02-d38ab375396d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1420_1421_AddTeamMemberPage/C2SC_Verify_error_message_on_not_entering_the_Email_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cdf90a5d-9218-4e9d-a96f-692e32b81793</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1420_1421_AddTeamMemberPage/C2SC_Verify_that_the_user_able_to_Add_a_new_Team_member_for_inspection</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>802a2463-4011-4e84-b749-a8e326bafb9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1420_1421_AddTeamMemberPage/C2SC_Verify_that_user_able_to_navigate_to_Add_Team_member_page_from_My_profile_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40797ad2-3fa0-4183-8dbe-f0378613efff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1420_1421_AddTeamMemberPage/C2SC_Verify_the_error_message_is_displayed_for_contact_name_field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c95aa35-8c2e-48c3-a6f5-d1c804115647</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1420_1421_AddTeamMemberPage/C2SC_Verify_user_able _to_add_team_member_for_Attorney</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c1c4b9b-e30b-4a91-8c9a-ddaba84952b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1420_1421_AddTeamMemberPage/C2SC_Verify_user_able_to_add_team_members_for_others_category</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
