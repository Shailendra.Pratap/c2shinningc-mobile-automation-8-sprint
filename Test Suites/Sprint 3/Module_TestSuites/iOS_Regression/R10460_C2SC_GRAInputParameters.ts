<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_GRAInputParameters</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>137f91cd-3c4d-49cf-b33c-c716561dd4dd</testSuiteGuid>
   <testCaseLink>
      <guid>2f4b4618-ed12-4d9d-8658-400981bf56b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1541_1551_GRAInputParameters/C2SC_To_verify_contents_displayed_in_mortgage_provider_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2450358-3eed-47d5-8274-7ee9e1f05d72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1541_1551_GRAInputParameters/C2SC_To_verify_whether_on_clicking_accept_option_recommended_Service_Provider_is_displayed_beside_the_milestone</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
