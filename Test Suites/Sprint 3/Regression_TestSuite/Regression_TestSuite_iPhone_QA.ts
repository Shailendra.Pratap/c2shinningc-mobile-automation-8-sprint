<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteCollectionEntity>
   <description></description>
   <name>Regression_TestSuite_iPhone_QA</name>
   <tag></tag>
   <executionMode>PARALLEL</executionMode>
   <maxConcurrentInstances>8</maxConcurrentInstances>
   <testSuiteRunConfigurations>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 3/Module_TestSuites/iOS_Regression/R10460_C2SC_AddTeamMemberPage</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 3/Module_TestSuites/iOS_Regression/R10460_C2SC_CreateEditViewDeleteToDo</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 3/Module_TestSuites/iOS_Regression/R10460_C2SC_EditGRAScreen</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 3/Module_TestSuites/iOS_Regression/R10460_C2SC_GRAInputParameters</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 3/Module_TestSuites/iOS_Regression/R10460_C2SC_LoginScreen</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 3/Module_TestSuites/iOS_Regression/R10460_C2SC_NewPasswordPage</testSuiteEntity>
      </TestSuiteRunConfiguration>
   </testSuiteRunConfigurations>
</TestSuiteCollectionEntity>
