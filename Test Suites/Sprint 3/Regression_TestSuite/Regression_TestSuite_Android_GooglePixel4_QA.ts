<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteCollectionEntity>
   <description></description>
   <name>Regression_TestSuite_Android_GooglePixel4_QA</name>
   <tag></tag>
   <executionMode>SEQUENTIAL</executionMode>
   <maxConcurrentInstances>8</maxConcurrentInstances>
   <testSuiteRunConfigurations>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 3/Module_TestSuites/Android_Regression/R10461_C2SC_AddTeamMemberPage</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 3/Module_TestSuites/Android_Regression/R10461_C2SC_CreateEditViewDeleteToDo</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 3/Module_TestSuites/Android_Regression/R10461_C2SC_EditGRAScreen</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 3/Module_TestSuites/Android_Regression/R10461_C2SC_GRAInputParameters</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 3/Module_TestSuites/Android_Regression/R10461_C2SC_LoginScreen</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 3/Module_TestSuites/Android_Regression/R10461_C2SC_NewPasswordPage</testSuiteEntity>
      </TestSuiteRunConfiguration>
   </testSuiteRunConfigurations>
</TestSuiteCollectionEntity>
