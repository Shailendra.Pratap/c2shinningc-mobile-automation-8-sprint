<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10758_C2SC_ScreentipFeatures</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>6fd3182d-e684-492e-914f-6353a436ae99</testSuiteGuid>
   <testCaseLink>
      <guid>09e58018-72a4-4cfc-a53f-588014bf0b67</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_338_2123_ScreenTipsFeatures/C2SC_Verify_consumer_is_able_ to_ view _the_description_when_ he _focuses_ on_ the_feature_Contract</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>758e8e1b-af3e-4b16-bb21-e64b74a76316</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_338_2123_ScreenTipsFeatures/C2SC_Verify_consumer_is_able_to_view_ the_basic_functionalities_of_the_home_photo_by_clicking_on the_guide_icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>213bd34f-09df-4851-8062-2ffe9730696a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_338_2123_ScreenTipsFeatures/C2SC_Verify_consumer_is_able_to_view_the_ basic_functionalities_ of_the_different_features in _the_app_by_clicking_the_guide_icon_on_the_top</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
