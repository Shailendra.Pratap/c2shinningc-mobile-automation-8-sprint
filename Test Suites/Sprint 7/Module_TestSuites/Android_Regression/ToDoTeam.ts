<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ToDoTeam</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>aa6931db-f49f-4860-b986-581e94648912</testSuiteGuid>
   <testCaseLink>
      <guid>93196e9a-b07a-4efa-90e5-429c9ad83b47</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2110_TodoTeamDropdown/C2SC_To_verify_self_added_GRA_contact_name_display_in_team_dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dff3ffc5-ef08-4dd2-8e2e-082048730dc2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2110_TodoTeamDropdown/C2SC_To_verify_consumer_can_edit_Team_member_on_editing_ToDo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
