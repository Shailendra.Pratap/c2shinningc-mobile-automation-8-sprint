<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10758_C2SC_Hompage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e7f19ba1-2941-4341-ade8-9635bf534e4d</testSuiteGuid>
   <testCaseLink>
      <guid>b69387b5-8ff9-464d-8199-ff3052023eb5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify the heading on top of My Docs page got changed from C2C to HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9581760-850a-4433-8f62-f4d2a1e6c2dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_Home_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9d5dae0-097f-45b9-ab79-b1259c2d9f11</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_My_Agent_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a556647-5379-4976-9683-66c71c310a76</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_My_Profile_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0b800ce-afb0-434f-b12b-cdcb42ebfe5f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_My_Team_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
