<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10758_C2SC_PropertyInsights</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>406c386c-5749-4a8c-b47d-c50725d4c387</testSuiteGuid>
   <testCaseLink>
      <guid>35516d5f-ee9a-422a-8d61-ee65fb722ec0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2185_2184_SelectedServiceProviderProperyPageEditable/To_verify_consumer_navigate_to_Property_Insight_screen_on_click_of_Edit_button_beside_Insight_header</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94ce27c4-6b8b-4ffa-805f-ab1b0d9ede0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2185_2184_SelectedServiceProviderProperyPageEditable/To_verify_content_of_Property_Insight_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6292ab00-f839-4d64-aaa4-86617dc9b9c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2185_2184_SelectedServiceProviderProperyPageEditable/To_verify_entered_data_in_Property_Insight_screen_is_saved</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
