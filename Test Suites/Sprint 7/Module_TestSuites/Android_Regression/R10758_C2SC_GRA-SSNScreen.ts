<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10758_C2SC_GRA-SSNScreen</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>d0c8d9a6-0333-48e9-941f-daadc911c2fb</testSuiteGuid>
   <testCaseLink>
      <guid>9a60dc89-54c1-43cc-933e-b9d97685dd44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1541_1551_GRAInputParameters/C2SC_Verify_consumer_is_able_to_see_asterisk_as_he_enters_the_SSN_value_after_selecting_the_preferred_mortgage_provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>131e2c48-f780-47d5-85a7-805216ad4819</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1541_1551_GRAInputParameters/C2SC_Verify_consumer_is_able_to_see_stars_when_he_finishes_entering_the_SSN_value_after_selecting_the_preferred_mortgage_provider</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
