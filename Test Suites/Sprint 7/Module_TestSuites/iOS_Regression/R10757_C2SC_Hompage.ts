<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10757_C2SC_Hompage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>8da4cf90-e2a1-4eff-b740-f8617ccf8aec</testSuiteGuid>
   <testCaseLink>
      <guid>e23d3028-a551-42a6-b43f-543c4339ea77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_Home_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>def6bcc5-96f6-4dc5-a9b3-cb5321c14e87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_My_Agent_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b5061cbb-e722-43aa-9e65-433511eb82ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_My_Profile_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>539a774f-b01d-414f-af0e-c24494e26fe1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_My_Team_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f19baad3-d348-460b-82d3-0c498b3c73c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify the heading on top of My Docs page got changed from C2C to HomePlace</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
