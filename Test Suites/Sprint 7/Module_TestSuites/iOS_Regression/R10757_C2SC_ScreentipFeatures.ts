<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10757_C2SC_ScreentipFeatures</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>0ef49357-4a20-4903-ae12-83bf257a3c1b</testSuiteGuid>
   <testCaseLink>
      <guid>9df423f3-eca8-4731-a21d-b4efd2cde6a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_338_2123_ScreenTipsFeatures/C2SC_Verify_consumer_is_able_ to_ view _the_description_when_ he _focuses_ on_ the_feature_Contract</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aadb02fc-2c53-4c62-9996-b88d3a877ea9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_338_2123_ScreenTipsFeatures/C2SC_Verify_consumer_is_able_to_view_ the_basic_functionalities_of_the_home_photo_by_clicking_on the_guide_icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ea999c83-8345-4ec5-a369-ddcfa648df9d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_338_2123_ScreenTipsFeatures/C2SC_Verify_consumer_is_able_to_view_the_ basic_functionalities_ of_the_different_features in _the_app_by_clicking_the_guide_icon_on_the_top</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
