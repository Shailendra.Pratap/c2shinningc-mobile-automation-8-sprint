<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10757_C2SC_SelectedServiceProviderProperyPageEditable</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>c2c823a5-9a17-46f2-9d0b-600fa9a0c0a8</testSuiteGuid>
   <testCaseLink>
      <guid>bf6b7002-61e4-49ea-a49a-5d6f4e6e49c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2185_2184_SelectedServiceProviderProperyPageEditable/To_verify_consumer_navigate_to_Property_Insight_screen_on_click_of_Edit_button_beside_Insight_header</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0f3ae5a-46e7-450e-b10a-f6219c489cb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2185_2184_SelectedServiceProviderProperyPageEditable/To_verify_content_of_Property_Insight_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9084a319-a7c8-474b-9437-1c923d7e6689</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2185_2184_SelectedServiceProviderProperyPageEditable/To_verify_entered_data_in_Property_Insight_screen_is_saved</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
