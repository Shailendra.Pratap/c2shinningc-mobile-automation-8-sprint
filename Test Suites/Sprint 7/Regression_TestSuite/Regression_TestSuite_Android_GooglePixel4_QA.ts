<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteCollectionEntity>
   <description></description>
   <name>Regression_TestSuite_Android_GooglePixel4_QA</name>
   <tag></tag>
   <executionMode>SEQUENTIAL</executionMode>
   <maxConcurrentInstances>8</maxConcurrentInstances>
   <testSuiteRunConfigurations>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 7/Module_TestSuites/Android_Regression/R10758_C2SC_MyTeam</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 7/Module_TestSuites/Android_Regression/R10758_C2SC_GRA-SSNScreen</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 7/Module_TestSuites/Android_Regression/R10758_C2SC_PropertyInsights</testSuiteEntity>
      </TestSuiteRunConfiguration>
   </testSuiteRunConfigurations>
</TestSuiteCollectionEntity>
