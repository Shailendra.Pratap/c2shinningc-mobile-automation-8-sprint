<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteCollectionEntity>
   <description></description>
   <name>Regression_TestSuite_Android_GooglePixel5_QA</name>
   <tag></tag>
   <executionMode>SEQUENTIAL</executionMode>
   <maxConcurrentInstances>8</maxConcurrentInstances>
   <testSuiteRunConfigurations>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/Android_Regression/R10461_C2SC_AddressMapDetails</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/Android_Regression/R10461_C2SC_AppLayout</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>false</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/Android_Regression/R10461_C2SC_AuthenticationPage</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/Android_Regression/R10461_C2SC_ConsumerFinancials</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/Android_Regression/R10461_C2SC_ConsumerFinancialScreen</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/Android_Regression/R10461_C2SC_CosumerDetailScreen</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/Android_Regression/R10461_C2SC_MyAgentPage</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/Android_Regression/R10461_C2SC_MyProfilePage</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/Android_Regression/R10461_C2SC_TellUsLittleAboutYouScreen</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>Android_Google_Pixel_5_10.0_QA</profileName>
            <runConfigurationId>google pixel 5</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/Android_Regression/R10461_C2SC_TermUse_Privacy</testSuiteEntity>
      </TestSuiteRunConfiguration>
   </testSuiteRunConfigurations>
</TestSuiteCollectionEntity>
