<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteCollectionEntity>
   <description></description>
   <name>Regression_TestSuite_iPhone_QA</name>
   <tag></tag>
   <executionMode>PARALLEL</executionMode>
   <maxConcurrentInstances>8</maxConcurrentInstances>
   <testSuiteRunConfigurations>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/iOS_Regression/R10460_C2SC_AddressMapDetails</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/iOS_Regression/R10460_C2SC_AppLayout</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/iOS_Regression/R10460_C2SC_AuthenticationPage</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/iOS_Regression/R10460_C2SC_ConsumerFinancials</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/iOS_Regression/R10460_C2SC_ConsumerFinancialScreen</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/iOS_Regression/R10460_C2SC_CosumerDetailScreen</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/iOS_Regression/R10460_C2SC_MyAgentPage</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/iOS_Regression/R10460_C2SC_MyProfilePage</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/iOS_Regression/R10460_C2SC_TellUsLittleAboutYouScreen</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 1/Module_TestSuites/iOS_Regression/R10460_C2SC_TermUse_Privacy</testSuiteEntity>
      </TestSuiteRunConfiguration>
   </testSuiteRunConfigurations>
</TestSuiteCollectionEntity>
