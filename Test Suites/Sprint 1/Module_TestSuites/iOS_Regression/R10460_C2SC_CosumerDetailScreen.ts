<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_CosumerDetailScreen</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>906235b6-1002-4551-ae24-078f389b1b32</testSuiteGuid>
   <testCaseLink>
      <guid>9170903a-8fa6-49dd-9823-0cadf889b537</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_466_ConsumerDetailScreen/C2SC_Test_to_check_the_details_shown_in_profile_validation_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fdc62da8-f620-46ef-8f51-0065abf8d861</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_466_ConsumerDetailScreen/C2SC_Test_to_check_the_Tell_us_about_you_page_after_selecting_the_set_up_my_profile_option</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
