<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_AddressMapDetails</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b389477f-234b-4e41-b617-b461c31052d4</testSuiteGuid>
   <testCaseLink>
      <guid>ad80a1e2-0f7d-40ac-8146-8b7f358dbaf9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_480_Address_Map_Details/C2SC_Test_to_check_the_map_option_is_navigating</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>020ac39e-15fd-4542-ad87-9d91a5933ca9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_480_Address_Map_Details/C2SC_Test_to_check_the_map_option_is_shown_in_the_My_Profile_details_page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
