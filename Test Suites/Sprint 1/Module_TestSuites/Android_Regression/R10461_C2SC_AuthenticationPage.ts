<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_AuthenticationPage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>a0496d31-9e28-4fbc-b513-4073c0f7e08b</testSuiteGuid>
   <testCaseLink>
      <guid>c57233dd-43d6-4231-b549-71a0184cd3d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_467_AuthenticationPage/C2SC_Verify_user_able_to_view_the_Authentication_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d0b9f2c-15c4-47ab-ab0b-8b4b60b2e86c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_467_AuthenticationPage/C2SC_Verify_user_able_to_view_profile_validation_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf72324c-bb2e-4ece-8df3-4a049b4a44f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_467_AuthenticationPage/C2SC_Verify_the_user_able_to_enter_the_mandatory_details_in_Authentication_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ceb536a3-cd04-4c3a-8600-f4cbc24a52c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_467_AuthenticationPage/C2SC_Verify_the_error_message_displayed_on_entering_an_invalid_code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d9270116-0a4c-4b59-9c53-20cf8ab0fdff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_467_AuthenticationPage/C2SC_Verify_the_alert_message_is_displayed_on_clicking_Dont_have_an_auth_code_option</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
