<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_TellUsLittleAboutYouScreen</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>8dfc63e9-ffae-4733-afb4-d91d79e5ee38</testSuiteGuid>
   <testCaseLink>
      <guid>5935358d-a49d-4092-943e-49c5161811c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_485_TellUsLittleAboutYouScreen/C2SC_Verify_that_the_user_navigates_to_homepage_by_clicking_Next_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e802df42-6783-480a-9a0f-becc2e467abd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_485_TellUsLittleAboutYouScreen/C2SC_Verify_that_the_user_navigates_to_homepage_by_clicking_skip_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e02c7b8f-c4f7-44cc-8b9e-90d5faa91361</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_485_TellUsLittleAboutYouScreen/C2SC_Verify_the_contents_in_Tell_us_about_you_page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
