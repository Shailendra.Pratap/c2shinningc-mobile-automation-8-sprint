<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10111_C2SC_ProfilePage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>ba4607ed-9ccd-4370-8ac7-dd344ba3d69b</testSuiteGuid>
   <testCaseLink>
      <guid>3014283c-5061-42c9-bf26-ca9129e723ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_488_884_ProfilePage/C2SC_Verify_fields_under_your_information_section_are_editable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6ed97009-bbdb-4913-8533-b4a2f6dda563</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_488_884_ProfilePage/C2SC_Verify_fields_under_you_are_buying_section_are_not_editable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>127d7df6-cc96-4d9c-9e36-bcb5640a118c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_488_884_ProfilePage/C2SC_Verify_buyer_is_redirected_to_tell_us_about_you_screen_on_click_of_ok_button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c7975b9-d9ba-4434-9fbc-a99e7876c965</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_488_884_ProfilePage/C2SC_Verification_of_tell_us_about_page_when_no_information_is_updated</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
