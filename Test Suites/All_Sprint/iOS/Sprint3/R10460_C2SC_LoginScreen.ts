<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10111_C2SC_LoginScreen</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>2dee9e37-db83-4675-8321-7034434a0305</testSuiteGuid>
   <testCaseLink>
      <guid>4763badb-fbd3-4ec3-af29-25fce225c317</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1425_1426_LoginScreen/C2SC_Test_to_check_the_login_page_entities_from_Registration_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>abb2af81-2494-45e1-a2b3-3fdcdd61b885</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1425_1426_LoginScreen/C2SC_Test_to_check_whether_buyer_is_redirected_back_to_registration_page_on_click_of_close_button</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
