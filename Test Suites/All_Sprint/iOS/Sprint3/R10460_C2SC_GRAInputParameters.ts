<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10111_C2SC_GRAInputParameters</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>33288da1-d115-4e86-9124-2d746652a5ce</testSuiteGuid>
   <testCaseLink>
      <guid>b799f58d-084d-4f48-91b5-041535d01623</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1541_1551_GRAInputParameters/C2SC_To_validate_the_error_message_when_invalidorblank_data_is_entered_in_input_fields_in_GRA_Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2f4b4618-ed12-4d9d-8658-400981bf56b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1541_1551_GRAInputParameters/C2SC_To_verify_contents_displayed_in_mortgage_provider_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2450358-3eed-47d5-8274-7ee9e1f05d72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1541_1551_GRAInputParameters/C2SC_To_verify_whether_on_clicking_accept_option_recommended_Service_Provider_is_displayed_beside_the_milestone</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
