<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10111_C2SC_EditGRAScreen</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b8104568-cb79-480b-8c20-a095cf96bbe8</testSuiteGuid>
   <testCaseLink>
      <guid>a1cff1cc-dc1b-4dba-9c58-45f48ebe5e31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_776_910_EditGRAScreen/C2SC_validate_the_error_message_displayed_when_blank_invalid_data_is_entered_in_input_fields</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f34ee620-1e62-42a7-9751-6fb05c14167d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_776_910_EditGRAScreen/C2SC_validate_display_of_company_name_updated_via_edit_option _besides_Inspection_milestone_on_my_teams_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b70838bf-fd14-4ece-8d36-76d79661e90c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_776_910_EditGRAScreen/C2SC_validate_display_of_company_name_updated_via_edit_option _besides_Insurance_milestone_on_my_teams_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9203397-b18b-42b8-bf29-177f79b374ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_776_910_EditGRAScreen/C2SC_validate_display_of_company_name_updated_via_edit_option _besides_mortgage_milestone_on_my_teams_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1dcc6285-1319-4efd-8783-a0c3a2532167</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_776_910_EditGRAScreen/C2SC_validate_display_of_company_name_updated_via_edit_option_besides_Title_milestone_on_my_teams_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75b7e801-dcca-42f8-8ccf-699203e73385</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_776_910_EditGRAScreen/C2SC_verify_navigation_to_edit_team_member_page_on_clicking_edit_option_from_default_milestone</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a56f3f38-df76-42dd-a758-e95d5b2ec886</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_776_910_EditGRAScreen/C2SC_verify_navigation_to_edit_team_member_page_on_clicking_edit_option_from_Inspection_milestone</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
