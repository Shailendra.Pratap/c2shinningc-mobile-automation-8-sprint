<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10196_C2SC_EditDeleteTeamMembers</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>d9477596-d12f-4d25-b10c-840a445c0af6</testSuiteGuid>
   <testCaseLink>
      <guid>5f1f71f9-2735-406a-a897-615752abcbd3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify _that _customer_ should_ be _able _to_ delete_ the_ recommended_ service_ provider_ information</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d90a4d66-f7ed-4ed0-9d97-2f0e21142707</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify _that _on _clicking _x_ the _buyer_ is _redirected _to _the _My Teams_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae5ee739-d365-46f9-b7f3-e8789d834483</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify _that_ customer _should_ be_ able_ to_ delete_ the _newly_ added_ custom_ team_ members</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6ccc161a-cc60-4f10-974f-6c1555f40304</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify _the_ discard_ option _when_ no_ changes_ are _made_ for_ newly _added_ custom_ team_ members</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78882dd5-df37-4ed9-86bc-62214ecc2b37</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify _the_ Recommended _Service_ Provider_ under _My _Teams</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8edff55b-ce29-4be7-9fa2-6a45fb5bd056</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_ that _customer _can_ be _able_ to _edit_ the_ preferred _vendor_ details_ for _recommended _service _provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>24173b98-d527-4ad7-b512-b5389eccc669</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_ the _cancel _option or close(x) _option_ for_ recommended _service_ provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1d5af76-447f-4134-ad0b-9c88594e0e14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_ the _contents_for _custom _team _members_self_added</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bcc03bb9-763e-47ac-ae05-cc15bfb7b2a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_ the _discard _option_ when _no changes _are_ made _for_ recommended _service_ provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>080c2cda-8012-4860-9deb-d546492c8d2b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_ the _Save _in _the_ edit_ popup_ for_ recommended _service_ provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9ed63d76-a41a-4e11-a40f-90a414cc369a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_ the_ discard and cancel_ in_ close_ popup _for_ recommended_ service_ provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2aecb7a7-f044-4ce0-b1c6-be9e3bca0c34</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_the _discard_ and _cancel_ in _close _popup _for _newly _added_ custom_ team_ members</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af4d7dc4-e074-41ec-8c6a-4bd6f114ed5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_the_ cancel_option or close(x) _option _for_newly_ added _custom _team _members</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>acdba64b-7698-42b4-b027-b60f6853e363</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_the_Cancel in the delete popup for recommended service provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f259f00f-3e24-44ef-9865-c5d5e297bf23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_the_Cancel_in_the_delete_popup_for_the_newly _added_custom_team_members</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a38d31b-f41b-462c-b2df-5d371b0bdcaf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_the_Save_in_ the_ edit_ popup_for _newly_ added _custom _team_ members</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
