<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10196_C2SC_MyTeamViewContactDetails</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>17b9237b-0543-4c76-95ad-e1ba8cb339e0</testSuiteGuid>
   <testCaseLink>
      <guid>8cd4a76b-33ce-4de8-8b82-3ee26d382d33</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To verify the team member details page if GRA is not Realogy affiliated</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>366808c6-4f46-456f-81b8-df5eba33cb58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To verify the team member details page if Inspection is selected from dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c50dc4d-2bb8-4410-b8bc-b33fc22e9ea3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To verify the team member details page if Other is selected in dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9e0f773-7979-4dcd-ae25-7424514cde34</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To verify the team member details page if service provider is GRA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15eecb7f-cbc0-4b19-a3be-db3a88d385ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To verify the team member details page if service provider is Home Owners insurance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8422e00d-fab4-48a8-a9b2-eb6d547617c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To_verify_the_team_member_details_page_if_Attorney_is_selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a129f852-5b87-4e57-9b45-c0c59924e0dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To_verify_the_team_member_details_page_if_service_provider_is_Title_or_Closing</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
