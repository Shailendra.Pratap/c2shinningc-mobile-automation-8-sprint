<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10196_C2SC_Homepage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>93145f99-43e0-434c-9b5a-2cb97ee81936</testSuiteGuid>
   <testCaseLink>
      <guid>93396ba2-0d89-40d4-b895-a5c6e0c50f4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_Mark as_done_under_submit_your_initial_earnest_money</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae255f29-a961-4b75-bdc2-9045fc0fa8b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_appearance_of_obtain_homeowners_insurance_and_closing_documents_in_to_do_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e67930cb-f14e-4dfa-903b-027bf62e3a45</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_appearance_of_Submit_your_initial_earnest_money</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>126f412e-80a4-4f34-9e1d-98dc4c926a1b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_close_icon_X_in_schedule_inspection_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d869b4a-42e9-40ef-907e-ceec0ff87bc8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Date_field_in_the_form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d86dc4e-3d93-45bb-9475-e5efb9b8eb9d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_error_message_for_Time_field in_schedule_inspection_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3034501b-08e4-41c4-928f-9ee16a3765c4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_fields_shown_for_closing_documents_task_in_to_do</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a199f90-e63d-4f5b-992f-efc67c90933b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_fields_shown_for_obtain_homeowners_insurance_task_in_to_do</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2571b8ce-0c9c-42c1-aa7b-2a0b1dd0ebf3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_fields_shown_for_Title_commitment_form_task_in_to_do</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04808f6b-064b-4f88-9587-900e6908f71e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Inspection_Company_field_in_the_form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60cc4692-7993-401a-98df-ebda6dede7ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_options_under_submit_your_initial_earnest_money</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c318150-4d6f-4898-87d4-a15872d74c42</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Schedule_Inspection_on_clicking_of_done</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a68a6280-3904-4416-9e68-81faabc1dec9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_sequence_in_Todo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d51a5dd7-cbc8-438d-942f-c07b1a55c872</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_verify_the_status_on_click_of_Obtain_Homeowners_insurance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c3795091-c97a-4888-bc2f-0643298189df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Time_field_in_the_form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3236ba58-0ee1-46b6-8982-b05a7e4e9b1d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Title_description_due_days_for_Submit_your_initial_earnest_money_task_in_todo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
