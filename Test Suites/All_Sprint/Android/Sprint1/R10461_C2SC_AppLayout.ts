<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_AppLayout</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>af532192-3b9e-4fc2-a366-6b8c52098c38</testSuiteGuid>
   <testCaseLink>
      <guid>ba1ff3f2-820b-4476-8008-49e2adef9474</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_472_AppLayout/C2SC_Verify_the_bottom_navigation_entities</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>612b357e-52ac-4ffc-a050-6d30248fe414</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_472_AppLayout/C2SC_Verify_the_home_screen_entities</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>794df1fb-3d38-45ec-a33b-27c98f4ef09d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_472_AppLayout/C2SC_Verify_the_top_navigation_entities</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
