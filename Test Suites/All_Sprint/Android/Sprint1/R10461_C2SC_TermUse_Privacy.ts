<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10341_C2SC_TermUse_Privacy</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>ff4320f5-08f5-42ab-818a-0fe383a1019f</testSuiteGuid>
   <testCaseLink>
      <guid>9fdc84df-cdab-4ca7-94cb-16e148fec3ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_337_TermUse_Privacy/C2SC_TermUsePrivacy_Verify_the_terms_and_conditions_page_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c1db293-4d21-43e6-b0ed-07ae5a4009f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_337_TermUse_Privacy/C2SC_Verify_the_Privacy_policy_page_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d41dc0c-1371-43aa-9248-6bfce5af954a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_337_TermUse_Privacy/C2SC_Verify_the_user_is_redirected_back_to_Authentication_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e694eb6-82e5-45b0-b815-27d3ab8e16d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_337_TermUse_Privacy/C2SC_Verify_the_user_is_redirected_back_to_authentication_page_from_privacy_policy_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3bbe3e60-13f8-4477-9375-c8743be247c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_337_TermUse_Privacy/C2SC_Verify_user_able_to_view_terms_and_conditions_from_Profile_page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
