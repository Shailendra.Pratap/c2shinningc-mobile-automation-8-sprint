<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10111_C2SC_AddTeamMemberORServiceProvider</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>83031725-785a-4945-85f8-f58df40fec8d</testSuiteGuid>
   <testCaseLink>
      <guid>a3d87bf2-199c-4f00-a12c-6a8bc340bef9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_895_773_AddTeamMemberORServiceProvider/C2SC_Verify_when_buyer_adds_team_member_name_and_category_display _on My_Teams_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c026384c-47c3-44de-8b81-bc3d2bc85384</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_895_773_AddTeamMemberORServiceProvider/C2SC_Verify_on_click_of_Close_icon,_buyer_is_redirected_to_My_Team_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe5d932b-9faa-47c2-a5c0-081e932c12c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_895_773_AddTeamMemberORServiceProvider/C2SC_Verify_error_message_is_thrown_when_input_fields_are_blank_and_click_plus_icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b09b8ede-2c48-4707-8304-ca4fccdb9ff0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_895_773_AddTeamMemberORServiceProvider/C2SC_Verify_buyer_is_navigated_to_Add_Team_Member_screen_on_clicking_Plus_icon_on_My_Teams_screen</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
