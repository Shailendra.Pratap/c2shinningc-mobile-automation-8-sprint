<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10095_C2SC_ForgotEmail</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>c9b21689-33e8-4532-af83-1b91cb3138c5</testSuiteGuid>
   <testCaseLink>
      <guid>df97b486-937e-43c1-96cd-be087144fc67</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2127_2128_ForgotEmail/C2SC_Verify_the_error_message_for_invalid_code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1ed8a0c0-144e-473f-9ef2-bb4e249ac11e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2127_2128_ForgotEmail/C2SC_Verify_the_error_message_if_the_invalid_mobile_number_entered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>230e636e-b811-4b86-9261-539bb038cef2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2127_2128_ForgotEmail/C2SC_Verify_the_Forgot_email_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63ce3e52-e0cb-4cd1-9b3b-e32d41cd897b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2127_2128_ForgotEmail/C2SC_Verify_the_retrieve_email</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
