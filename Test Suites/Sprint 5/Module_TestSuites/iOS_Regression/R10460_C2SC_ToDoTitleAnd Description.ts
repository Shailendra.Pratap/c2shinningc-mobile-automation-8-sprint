<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_ToDoTitleAnd Description</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>d084a077-b2e1-456b-97cc-6c4a696f842e</testSuiteGuid>
   <testCaseLink>
      <guid>0db82d67-d881-495e-abc9-a4a3a3a86fe3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_443_2855_ToDoTitleAnd Description/C2SC_Verify_Closing_Documents_Available_description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d6fef289-c41b-4510-91d2-6be3ae79a3ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_443_2855_ToDoTitleAnd Description/C2SC_Verify_Obtain_HomeOwners_Insurance_title_and _description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c7b90e02-c2e3-4731-93b2-f22aae6f2b51</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_443_2855_ToDoTitleAnd Description/C2SC_Verify_Schedule_Closing(Calender) _title_and _description</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f9a487da-8515-4a9e-88af-3d1b76925ea3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>bcbfec53-0cba-4633-9d9a-4d1e0ec65c9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_443_2855_ToDoTitleAnd Description/C2SC_Verify_Schedule_Inspection(Calender) _title_and _description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c4f0bb6c-d6b8-4d61-afde-6f7e12c078f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_443_2855_ToDoTitleAnd Description/C2SC_Verify_Submit_your_initial_earnest_money_(payment) _title_and _description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a63a9a64-3d74-43d9-9607-17280928d139</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_443_2855_ToDoTitleAnd Description/C2SC_Verify_Title_commitment_form_received_(pdf)_title_and_description</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
