<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_MyTeamViewContactDetails</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>98faf816-c8d6-47b6-b981-316de26d2c3f</testSuiteGuid>
   <testCaseLink>
      <guid>6f93894b-0ff9-41a2-a0be-8c58a7c5f7a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To verify the team member details page if GRA is not Realogy affiliated</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40cc7378-cac5-4fcc-a08d-51ebe4fc7866</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To verify the team member details page if Inspection is selected from dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2910e1a9-474e-4fb8-9b30-5a0bdcd38c83</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To verify the team member details page if Other is selected in dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0aa6ef4b-9ca4-4312-8eb8-e5bd774157eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To verify the team member details page if service provider is GRA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d5403b2-5eac-4cb8-9992-60c22ea5fd3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To verify the team member details page if service provider is Home Owners insurance</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
