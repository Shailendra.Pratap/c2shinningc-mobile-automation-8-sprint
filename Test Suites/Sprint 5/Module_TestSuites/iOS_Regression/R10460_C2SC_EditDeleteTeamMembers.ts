<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_EditDeleteTeamMembers</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e2a531f0-b7e1-45f5-9c0d-32ee4475fa9d</testSuiteGuid>
   <testCaseLink>
      <guid>ba547a77-9593-4c8f-8b0c-235098f5ee09</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify _that _customer_ should_ be _able _to_ delete_ the_ recommended_ service_ provider_ information</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5218325c-7941-47c3-b212-8224d84c04ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify _that _on _clicking _x_ the _buyer_ is _redirected _to _the _My Teams_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf1868cb-9331-4b4e-8676-21416ca3629b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify _that_ customer _should _be _able_ to_ delete _the _Preferred _Service_ Provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1918fcb4-acb7-461b-ae8d-6f9cdfe91c00</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify _that_ customer _should_ be_ able_ to_ delete_ the _newly_ added_ custom_ team_ members</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4304e3a6-b2c6-42d9-8882-7912003568ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify _the_ discard_ option _when_ no_ changes_ are _made_ for_ newly _added_ custom_ team_ members</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>002266ca-8ac5-45f1-bdaf-5a33f5f70b4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify _the_ Recommended _Service_ Provider_ under _My _Teams</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fb175e36-2a7b-49e4-a51e-68847f428f41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_ that _customer _can_ be _able_ to _edit_ the_ preferred _vendor_ details_ for _preferred_service _provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>57f8b340-b785-4c3c-9085-6a3e10753463</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_ that _customer _can_ be _able_ to _edit_ the_ preferred _vendor_ details_ for _recommended _service _provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>70f3766a-683f-48c2-8ba5-5f48a9b572d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_ the _cancel _option or close(x) _option_ for_ recommended _service_ provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36afbd2e-3370-4fa7-b01a-4d62ec14c298</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_ the _contents_for _custom _team _members_self_added</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0fb41f91-0f32-4f50-9698-cafe64faad7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_ the _discard _option_ when _no changes _are_ made _for_ recommended _service_ provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7266a3f6-1883-4fbc-a72d-efcd1b49b1ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_ the _Save _in _the_ edit_ popup_ for_ preferred_service_ provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>890e630f-45a9-418f-bf17-eb6d70721f16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_ the _Save _in _the_ edit_ popup_ for_ recommended _service_ provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33486f7a-3d1d-4c1b-87d5-98505bcf0b85</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_ the_ discard and cancel_ in_ close_ popup _for_ recommended_ service_ provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad5e1cf5-40de-411d-bfcd-22b0f4227809</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_the _discard_ and _cancel_ in _close _popup _for _newly _added_ custom_ team_ members</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25f786df-f948-4acc-ac3e-6b8a81102d05</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_the_ cancel_option or close(x) _option _for_newly_ added _custom _team _members</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ddc14a5e-0930-4996-92dd-d551478912d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_the_Cancel in the delete popup for recommended service provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c0345cd3-fe72-440e-b8a0-0c7bce05bfb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_the_Cancel _in _the _delete_ popup_ for_preferred_ service_ provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3498ba2-8db8-4de5-8302-eb1d6a4b5611</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_the_Cancel_in_the_delete_popup_for_the_newly _added_custom_team_members</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cdc08791-fbfa-4f33-a3fb-a7961276a109</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_925_926_EditOrDeleteTeamMemberDetails/C2SC_Verify_the_Save_in_ the_ edit_ popup_for _newly_ added _custom _team_ members</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
