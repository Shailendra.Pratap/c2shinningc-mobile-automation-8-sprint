<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_Homepage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>1e3a6bf5-1f22-4f46-9b55-7245caf0132a</testSuiteGuid>
   <testCaseLink>
      <guid>c848401a-3454-44b1-ab9f-113707b83b3e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_appearance_of_obtain_homeowners_insurance_and_closing_documents_in_to_do_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b83963c-f789-445b-8134-1e348acf5639</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_close_icon_X_in_schedule_inspection_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ed4ca79-fea4-48f6-a2de-9fecbb6ff1a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Date_field_in_the_form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>91809fcb-5f37-434b-b7ae-70cd9b53ba56</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_error_message_for_Time_field in_schedule_inspection_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd6d53fb-a2d7-4553-9a4b-c826d6a97217</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_fields_shown_for_closing_documents_task_in_to_do</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50283ec5-c379-4681-bc5e-c907cd29ff14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_fields_shown_for_obtain_homeowners_insurance_task_in_to_do</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>afaf4571-1087-4abd-8ad4-591375a8e2c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_fields_shown_for_Title_commitment_form_task_in_to_do</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ea141d31-978b-4117-9e38-fd3e83048efe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Inspection_Company_field_in_the_form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab3fd1d0-babd-41d5-b6f9-3c2b0f1f0593</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Schedule_Inspection_on_clicking_of_done</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>68002c7d-3ce9-49d8-a5db-757e31b2d0ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_sequence_in_Todo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f989426-2de4-4b8d-b4e2-a2a78e8d01d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_verify_the_status_on_click_of_Obtain_Homeowners_insurance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41d81a75-c54c-4042-8660-56eaa3f92dcc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Time_field_in_the_form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>22b2a4f2-7760-49a6-84df-4f2f9aa09f3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Wire_Funds_for_closing_pdf_task_in_to_do</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
