<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_ScheduleClosingToDo</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>659126fa-975b-461f-a3b6-666e2956e333</testSuiteGuid>
   <testCaseLink>
      <guid>3b696a8b-c334-400b-8788-64bd7a597bc6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_3011_3012_ScheduleClosingToDo/C541196_C2SC_Verify_the_fields_and_respective_error_messages_in_schedule_closing_form</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>af22f764-2188-423b-bb81-49a0db67ab1b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f64b72ae-bcd7-4073-bd61-2cb19aafd872</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_3011_3012_ScheduleClosingToDo/C541197_C2SC_Verify_the_status_of_task_on_click_of_schedule</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>36644583-3dff-402c-9d2c-8246a656332a</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
