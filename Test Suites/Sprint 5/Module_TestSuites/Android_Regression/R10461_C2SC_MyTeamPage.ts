<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_MyTeamPage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>4bde8aa4-5b47-469b-84d5-d978f364651c</testSuiteGuid>
   <testCaseLink>
      <guid>cd22a7b1-fd49-449c-85fe-18250366fd6a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2886_2887_MyTeamPage/C2SC_Verify_the_error_message_for_Contact_name_field_in_Add_team_member_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d400944c-6a89-40cb-9f24-9b29d58d7cba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2886_2887_MyTeamPage/C2SC_Verify_the_error_message_for_Contact_name_field_in_Edit_team_member_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b0c23f8-0807-4168-8af8-d7cf35e5c4d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2886_2887_MyTeamPage/C2SC_Verify_the_error_message_for_email_Id_field_in_Add_team_member_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d42f961-c56c-4cca-bfda-b98ed0edc0c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2886_2887_MyTeamPage/C2SC_Verify_the_error_message_for_email_Id_field_in_Edit_team_member_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e2adc50-8f99-4683-b684-92246f9d06fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2886_2887_MyTeamPage/C2SC_Verify_the_error_message_for_Phone_field_in_Add_team_member_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>445003b7-d899-43fc-9feb-e0f888ab3629</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2886_2887_MyTeamPage/C2SC_Verify_the_error_message_for_Phone_field_in_Edit_team_member_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4afd3b2-494d-4c28-8501-9c48f8bc8e1c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2886_2887_MyTeamPage/C2SC_Verify_the_fields_in_Add_team_member_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3290c907-12de-428d-80f2-feacdce00151</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2886_2887_MyTeamPage/C2SC_Verify_the_fields_in_Edit_team_member_page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
