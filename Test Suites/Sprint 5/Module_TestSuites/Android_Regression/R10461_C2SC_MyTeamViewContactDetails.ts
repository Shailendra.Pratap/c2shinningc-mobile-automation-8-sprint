<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_MyTeamViewContactDetails</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>17b9237b-0543-4c76-95ad-e1ba8cb339e0</testSuiteGuid>
   <testCaseLink>
      <guid>b53a3e18-e655-418e-bf57-fd41292d8f0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To verify the team member details page if Attorney is selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>edd4885d-2d74-4a4f-957f-5f5b42e13b31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To verify the team member details page if GRA is not Realogy affiliated</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7758551b-918c-4868-b38a-537fc884b9f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To verify the team member details page if Inspection is selected from dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c6c49be-1806-4de2-84cf-10948b216427</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To verify the team member details page if Other is selected in dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>540dbd16-814d-4346-8166-3f7a62e5c4aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To verify the team member details page if service provider is GRA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5e5c684b-bed5-462e-94ae-8429a9bfda20</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To verify the team member details page if service provider is Home Owners insurance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e59c5a65-09fa-4ee6-9e57-db29a439577f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_914_915_MyTeamViewContactDetails/To verify the team member details page if service provider is Title or Closing</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
