<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_ToDoTitleAnd Description</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>24649bc9-f2b5-4fbd-9c01-8f2cfd6dbc2c</testSuiteGuid>
   <testCaseLink>
      <guid>89d54e3d-af96-4619-af77-6c1a7261e645</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_443_2855_ToDoTitleAnd Description/C2SC_Verify_Closing_Documents_Available_description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c909c57-c7dc-47a8-9068-d10ae4c066d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_443_2855_ToDoTitleAnd Description/C2SC_Verify_Obtain_HomeOwners_Insurance_title_and _description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f27f614f-02d1-4ad6-92cd-5527f847f621</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_443_2855_ToDoTitleAnd Description/C2SC_Verify_Schedule_Closing(Calender) _title_and _description</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f9a487da-8515-4a9e-88af-3d1b76925ea3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>20299f10-dee7-4a56-8938-cd6f756e4baa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_443_2855_ToDoTitleAnd Description/C2SC_Verify_Schedule_Inspection(Calender) _title_and _description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>09a3fc68-4370-4a33-94bc-7f6dae62dd8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_443_2855_ToDoTitleAnd Description/C2SC_Verify_Submit_your_initial_earnest_money_(payment) _title_and _description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b2ed7603-50d8-4238-8f0f-a3d6c2a9353b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_443_2855_ToDoTitleAnd Description/C2SC_Verify_Title_commitment_form_received_(pdf)_title_and_description</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
