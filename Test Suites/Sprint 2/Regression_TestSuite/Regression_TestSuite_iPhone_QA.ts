<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteCollectionEntity>
   <description></description>
   <name>Regression_TestSuite_iPhone_QA</name>
   <tag></tag>
   <executionMode>PARALLEL</executionMode>
   <maxConcurrentInstances>8</maxConcurrentInstances>
   <testSuiteRunConfigurations>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>false</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 2/Module_TestSuites/iOS_Regression/R10460_C2SC_AddTeamMemberORServiceProvider</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>false</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 2/Module_TestSuites/iOS_Regression/R10460_C2SC_AddToDoItemScreen</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>false</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 2/Module_TestSuites/iOS_Regression/R10460_C2SC_ConsumerDetails</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>false</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 2/Module_TestSuites/iOS_Regression/R10460_C2SC_HomePage</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>false</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 2/Module_TestSuites/iOS_Regression/R10460_C2SC_MyAgentDetails</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>false</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 2/Module_TestSuites/iOS_Regression/R10460_C2SC_MyAgentPage</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>false</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 2/Module_TestSuites/iOS_Regression/R10460_C2SC_MyTeamPage</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 2/Module_TestSuites/iOS_Regression/R10460_C2SC_ProfilePage</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Custom</groupName>
            <profileName>iPhone_12_14_QA</profileName>
            <runConfigurationId>iphone</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Sprint 2/Module_TestSuites/iOS_Regression/R10460_C2SC_TodoScreen</testSuiteEntity>
      </TestSuiteRunConfiguration>
   </testSuiteRunConfigurations>
</TestSuiteCollectionEntity>
