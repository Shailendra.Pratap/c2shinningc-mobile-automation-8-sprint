<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_MyAgentPage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>ecfc454b-5c48-4a7a-995a-0e0abf897beb</testSuiteGuid>
   <testCaseLink>
      <guid>f3f4701b-6def-4ac7-9082-16c3fb9187ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_748_Agent_Page/C2SC_Verify_the_Next_option_is_enabled_on_selecting_the_agent</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ca376bc3-6ff6-41a9-88ea-5cf756d8702a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_748_Agent_Page/C2SC_Verify_the_Next_option_is_disabled_if_user_not_selecting_an_agent</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ad31ae8-04a7-4f29-8b49-feda844fb986</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_748_Agent_Page/C2SC_Verify_the_Agent_selection_page_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dad3738c-99f2-425c-a649-c22fbb9201d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_748_Agent_Page/C2SC_Verify_buyer_is_redirected_to_the_profile_validation_page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
