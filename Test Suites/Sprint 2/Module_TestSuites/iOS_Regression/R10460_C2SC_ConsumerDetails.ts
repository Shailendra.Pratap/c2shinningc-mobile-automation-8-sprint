<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_ConsumerDetails</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>82072157-700f-41c6-889c-44ef4de4d0f9</testSuiteGuid>
   <testCaseLink>
      <guid>6df3ccd4-bfdf-497f-8cd2-e7cef15e9e4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_909_330_ConsumerDetails/C2SC_Verify_blank_space_is_shown_if_any_of_the_Consumer_details_are_unavailable_on_the_Consumer_details_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9fc8365a-4372-4745-a8c0-4415c69ef95c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_909_330_ConsumerDetails/C2SC_Verify_the_Consumer_details_getting_auto_populated_on_the_Consumer_details_screen</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
