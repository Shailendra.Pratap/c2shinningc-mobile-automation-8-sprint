<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_MyTeamPage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>ed205596-9cfe-4980-8ada-6d21ab75819c</testSuiteGuid>
   <testCaseLink>
      <guid>4d450873-10a5-48a5-ae20-06687e1cc988</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_312_MyTeamPage/C2SC_Verify_user_able_to_view_service_provider_beside_Home_Owners_Insurance_milestone</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>097c5652-edc5-47e2-aba9-b3753b967943</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_312_MyTeamPage/C2SC_Verify_User_able_to_select_the_service_provider_for_all_the_milestones</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5edf325a-1c5a-4964-ba8e-2489295ef897</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_312_MyTeamPage/C2SC_Verify_user_abl_view_service_provider_beside_the_Title_or_Closing_milestone</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59ed1791-a367-408f-9b48-6e708f1e7b5b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_312_MyTeamPage/C2SC_Verify_the_user_able_to_view_the_service_provider_beside_the_Mortgage_milestone</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
