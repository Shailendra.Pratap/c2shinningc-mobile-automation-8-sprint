<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_MyAgentDetails</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e46e7dfb-e5b5-4c94-bbea-6d7f5b7b2290</testSuiteGuid>
   <testCaseLink>
      <guid>373721cc-edf2-4b75-8d1e-71c4071eec96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_899_898_MyAgentDetails/C2SC_Verify_the_buyer_is_able_to_view_the_My_Agent_page_with_the_selected_agents_contact_details</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
