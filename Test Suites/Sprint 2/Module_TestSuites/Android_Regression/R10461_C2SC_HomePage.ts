<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_HomePage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>c86c63b6-2637-4786-8003-054496f5654a</testSuiteGuid>
   <testCaseLink>
      <guid>995f1473-95af-441e-8456-2a60bb224b80</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_563_Home_Page/C2SC_To_verify_task_category_contents</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>092cf582-2ef0-4237-be83-2bdf4f0642a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_563_Home_Page/C2SC_To_verify_Homepage_validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f6c40ebe-56fd-46f2-b125-2a5eca37b9be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_563_Home_Page/C2SC_To_verify_display_of_on_track_Icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b80c982-709a-48d5-b92a-04b16f4ea580</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_563_Home_Page/C2SC_To_verify_display_of_Closing_Day_in_place_of_estimated_difference</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fcb70265-7bd6-49a9-baa9-7807701c8a06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_563_Home_Page/C2SC_To_verify_display_of_closing_date_and_number_of_days_to_closing</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
