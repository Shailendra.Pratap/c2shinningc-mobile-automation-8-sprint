<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_AddToDoItemScreen</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>9e9e5564-3b9f-424e-b81a-05984bd3c779</testSuiteGuid>
   <testCaseLink>
      <guid>becac747-4282-4d00-be05-84fbf8de2d1f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/C2SC_273_890_AddToDoItemScreen/C2SC_Verify_when_buyer_clicks_Discard_button_buyer_should_be_navigated_to_Home_page_and_edited_task_is_discarded</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a546b340-9a7d-4137-8ed9-cc5127fbbe97</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/C2SC_273_890_AddToDoItemScreen/C2SC_Verify_when_buyer_clicks_Cancel_button,_buyer_should_stay_in_Add_Item_screen_and_edited_fields_remain_intact</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb4161af-0645-46c4-952c-cf460df3001f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/C2SC_273_890_AddToDoItemScreen/C2SC_Verify_when_Add_Task_form_is_saved,_that_task_should_be_listed_under_To_Do_tab_on_Home_Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4731f0c-c1fa-44b9-b986-a9aa7c8c0e09</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/C2SC_273_890_AddToDoItemScreen/C2SC_Verify_pop_up_message_is_displayed_when_buyer_filled_form_and_click_close or back_icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4f06933-8daf-467a-a155-6698096040b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/C2SC_273_890_AddToDoItemScreen/C2SC_Verify_on_click_of_close_icon_when_no_data_is_entered_buyer_should_navigate_to_My_Home_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>581ddc53-7e78-4b82-866b-bf733f37f2b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/C2SC_273_890_AddToDoItemScreen/C2SC_Verify_display_of_error_message_when_required_fields_not_added</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63ef74ff-22b8-4728-b98e-64e0c4fd34a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/C2SC_273_890_AddToDoItemScreen/C2SC_Verify_buyer_able_to_add_the_task_when_enter_data_all_input_fields</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20edacca-013a-42e3-849e-6ac784a31dea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/C2SC_273_890_AddToDoItemScreen/C2SC_Verify_all_fields_are_present_on_Add_Item_screen_as_per_design</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
