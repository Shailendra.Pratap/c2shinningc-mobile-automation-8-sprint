<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_TodoScreen</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>75c8d25d-8712-4ff6-ad97-0b8bb27d2855</testSuiteGuid>
   <testCaseLink>
      <guid>191be45c-31d0-4b73-9abb-ce4eca765e1a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_779_911_TodoScreen/C2SC_Verify_discarding_the_self_assigned_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1744d74f-a604-4a8b-bc9e-12dbc76c3805</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_779_911_TodoScreen/C2SC_Verify_marking_a_self_assigned_task_as_done</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7c4ea13-2311-41c4-a933-282697f37412</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_779_911_TodoScreen/C2SC_Verify_saving_the_self_assigned_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d66c5444-244a-459c-bfa3-771abc3f873b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_779_911_TodoScreen/C2SC_Verify_whether_the_user_is_able_to_view_the_header_and_number_of_to_do_tasks</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
