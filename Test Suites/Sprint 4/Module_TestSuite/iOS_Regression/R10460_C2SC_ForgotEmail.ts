<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_ForgotEmail</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>8b0b4cc0-3ccc-483a-baa8-1c90c2551447</testSuiteGuid>
   <testCaseLink>
      <guid>3ab73993-3cbe-4978-8401-1af939f174e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2127_2128_ForgotEmail/C2SC_Verify_the_error_message_for_invalid_code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a7b1f77f-f83a-49c4-a043-0280e5b930e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2127_2128_ForgotEmail/C2SC_Verify_the_error_message_if_the_invalid_mobile_number_entered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33c7d1db-c136-46b2-a8da-959615c89727</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2127_2128_ForgotEmail/C2SC_Verify_the_Forgot_email_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>17995ddc-4914-4209-9fd9-498bbc605cb8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2127_2128_ForgotEmail/C2SC_Verify_the_retrieve_email</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
