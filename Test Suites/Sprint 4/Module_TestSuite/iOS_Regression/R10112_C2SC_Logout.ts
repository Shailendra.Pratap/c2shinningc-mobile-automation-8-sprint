<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10112_C2SC_Logout</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>81028918-4c88-47e2-a76c-1fa227925673</testSuiteGuid>
   <testCaseLink>
      <guid>25ea14ae-513c-41c1-bcdc-94a2dd9aff65</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2171_494_Logout/C2SC_Verify_the_logout_functionality_of_C2C_app</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
