<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10112_C2SC_MyJourney</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b8862752-1814-4c12-88a7-1dcb5a8fe511</testSuiteGuid>
   <testCaseLink>
      <guid>795b6339-dcba-4aa5-a8c6-88ab4037e8ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1442_1567_MyJourney/C2SC_Verify_the_back_button_in_My_Journe_page_in_Homepage_top_right_corner</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e820ade-9065-4988-bf66-296acfbbbda5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1442_1567_MyJourney/C2SC_Verify_the_icons_in_My_Journey_page_in_Homepage_top_right_corner</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8df2eb9-5ebd-4b35-93de-1b4e3a927bc6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1442_1567_MyJourney/C2SC_Verify_the_My_Journey_button_content_in_Homepage_top_right_corner</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
