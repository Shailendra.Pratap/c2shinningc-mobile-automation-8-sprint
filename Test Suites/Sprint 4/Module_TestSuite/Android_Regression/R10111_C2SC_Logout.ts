<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10111_C2SC_Logout</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>9182ffc5-fd0d-406c-b0b2-d79240ba92fa</testSuiteGuid>
   <testCaseLink>
      <guid>e58c3f7d-f84a-4fa7-b498-fcf9dbdd6f6a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_2171_494_Logout/C2SC_Verify_the_logout_functionality_of_C2C_app</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
