<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10111_C2SC_MyJourney</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>97c76102-3621-449b-80f6-52011699e36a</testSuiteGuid>
   <testCaseLink>
      <guid>d7e4db3b-c03f-4178-a459-1e4b74082637</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1442_1567_MyJourney/C2SC_Verify_the_back_button_in_My_Journe_page_in_Homepage_top_right_corner</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>69cc471a-d286-4e61-8f26-b0d457ced22e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1442_1567_MyJourney/C2SC_Verify_the_icons_in_My_Journey_page_in_Homepage_top_right_corner</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c5a9cf22-e52b-4876-a7f4-158ee1999765</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_1442_1567_MyJourney/C2SC_Verify_the_My_Journey_button_content_in_Homepage_top_right_corner</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
